import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw',
  authDomain: 'opd-check-com.firebaseapp.com',
  projectId: 'opd-check-com',
  storageBucket: 'opd-check-com.appspot.com',
  messagingSenderId: '473581330102',
  appId: '1:473581330102:web:1415eceae267ee888bd017',
  measurementId: 'G-HL6YYGDVB1'
};
const secondaryFireApp = firebase.initializeApp(firebaseConfig, 'secondaryFireApp');
// const FormElementsPageApp = () => {
export default secondaryFireApp;
// };

// export default FormElementsPageApp;
