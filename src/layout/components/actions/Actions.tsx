import React ,{useState,useEffect}from 'react';

import './Actions.scss';

import Notifications from './Notifications';
import SettingsDropdown from './SettingsDropdown';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import {Tooltip} from  'antd';

const Actions = () => {

  const [hospName, setHospName] = React.useState([]);
 
  const [change, setChange] = useState(false);

  useEffect(() => {
    
    getData();

  }, []);

  const getData = () => {
    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log('user id receptionist', user.uid);

        let arr = [];
        // await Promise.all(data.hospitalName.map(async (hospId, idx) => {
        // new Promise.all(_.each(data.hospitalName, (hospId) => {

        await firebase.firestore()
          .collection("receptionist").where('userID','==',user.uid).get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              // console.log('doc', doc.data().createdOn.toDate());

              arr.push(doc.data());

            });
          });

        // }))
        console.log("arr", arr);
        setHospName(arr);

        setChange(true);
      }
    });
  };

  console.log('hospName', hospName);
  return(

  <div className='actions'>
    <Tooltip title="Hospital Name">
   <span style={{fontSize:'1.5rem',fontWeight:700,fontFamily:'sans-serif',marginRight:20,color:'#8f9091'}}>{hospName.length>0 && hospName[0].hospitalName}</span>
   </Tooltip>
    <Notifications />

    <SettingsDropdown />
  </div>
  )
};

export default Actions;
