import React,{useState,useEffect} from 'react';

import { usePageData } from '../../../hooks/usePage';
import { usePatients } from '../../../hooks/usePatients';

import PatientsTable from './PatientsTable';

import { IPageData } from '../../../interfaces/page';
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

const pageData: IPageData = {
  title: 'Doctors List',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Medicine',
      route: 'default-dashboard'
    },
    {
      title: 'Doctors'
    }
  ]
};

const PatientsPage = () => {
  const { patients, editPatient, deletePatient } = usePatients();
  // const location = useLocation();
  // let data = location.state;
  // usePageData(pageData);
  // console.log('props data', data);
  const [open, setOpen] = React.useState(false);

  // const [hospitalNameVal, hospitalName] = useState(data.hospitalName);
  // const [firstNameVal, firstName] = useState(data.firstName);
  // const [lastNameVal, lastName] = useState(data.lastName);
  // const [emailAddressVal, emailAddress] = useState(data.email);
  // const [passwordVal, password] = useState(data.code);
  // const [mobileNumberVal, mobileNumber] = useState(data.mobileNumber);

  const [change, setChange] = useState(false);
  
  const [addReceptionist, setReceptionist] = useState(false);
  const [editReceptionist, setReceptionistEdit] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  const [doctorList, setDoctorList] = useState([]);
  // const [doctorArr, setDoctorArr] = useState(data.doctors);
  const [changeArr, setChangeArr] = useState(false);
  const [receptionistArr, setReceptionistArr] = useState([]);
  const [recptionistDoctorArr, setRecptionistDoctorArr] = useState([]);
  const [receptionistEdit, setRecptionistEdit] = useState({});

  useEffect(() => {

    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user", user);
        let arrNew = [];
        await firebase.firestore()
        .collection("receptionist").where('userID','==',user.uid).get()
          .then((querySnapshot) =>{
            // console.log("querySnapshot",querySnapshot)
            querySnapshot.forEach( (doc)=> {

              console.log("doc receptionist", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arrNew.push(doc.data())

            });

          
            setChangeArr(true)
            setReceptionistArr(arrNew[0].doctors)

          })
        }
      })

  }, [])

  console.log("receptionistArr", receptionistArr)

  usePageData(pageData);

  return (
    <>
      <PatientsTable
        // onDeletePatient={deletePatient}
        // onEditPatient={editPatient}
        patients={receptionistArr}
      />
    </>
  );
};

export default PatientsPage;
