import React, { useState, useEffect } from 'react';
import { Avatar, Table, Button, Modal, Tag, Tooltip,Image } from 'antd';
import { ColumnProps } from 'antd/es/table';
import axios from "axios";
// import setting from '../../../config/setting'
import { IPatient } from '../../../interfaces/patient';
import { Spin, Space, Row, Col, Divider } from 'antd';
import { history } from '../../../redux/store';
import PatientForm from '../../../layout/components/patients/PatientForm';
import moment from 'moment';
import _ from 'underscore';

type Props = {
  patients: IPatient[];
  onEditPatient?: (patient: IPatient) => void;
  onDeletePatient?: (id: string) => void;
};

type PatientsImgProps = {
  img: string;
};

const PatientImg = ({ img }: PatientsImgProps) => {
  // const isData = img.startsWith('data:image');
  // const isWithPath = img.startsWith('http');

  // if (isData || isWithPath) {
  //   return <Avatar size={40} src={img} />;
  // }

  return <Avatar size={40} src={`${window.location.origin}/${img}`} />;
};

const PatientsTable = ({
  patients,
  onEditPatient = () => null,
  onDeletePatient = () => null
}: Props) => {
  const [patient, setPatient] = useState(null);
  const [visibility, setVisibility] = useState(false);
  const [employeeName, setEmployeeName] = useState([]);

  useEffect(() => {

   // getEmployee();

  }, [])

  const closeModal = () => setVisibility(false);

  const handleShowInfo = (patient) => {
    console.log("patient", patient);
    history.push({
      pathname: '/vertical/patient-profile'
      , state: patient
    });
  }

  // const handleTestHistory = (patientData) => {

  //   console.log("handleTestHistory", patientData);
  //   history.push({ pathname: `/vertical/employeeTestHistory`, state: patientData });

  // }

  const handleDeletePatient = (id) => onDeletePatient(id);
  const handleEditPatient = (patient: IPatient) => {
    setPatient(patient);
    setVisibility(true);
  };

  const actions = (data) => (
    <div className='buttons-list nowrap'>
      <Tooltip title="Employee Test History">
      {/* <Button shape='circle' onClick={handleShowInfo}>
        <span className='icofont icofont-external-link' />
      </Button> */}
      </Tooltip>
      <Tooltip title="Patients List">
        <Button shape='circle' onClick={() => handleShowInfo(data)} danger
          style={{
            background: 'none',
            border: 'none',
            boxShadow: 'none',
            color: 'rgb(233 109 109)'
          }}
        >
          <span className='fa fa-address-card' style={{ fontSize: 25 }} />
        </Button>
      </Tooltip>
      {/* <Button onClick={handleEditPatient.bind({}, patient)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button> */}
      {/* <Button onClick={handleDeletePatient.bind({}, patient.id)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button> */}
    </div>
  );

  
  const columns = [
    {
      key: 'fileUrl',
      dataIndex: 'fileUrl',
      title: 'Image',
      //sorter: (a, b) => a.firstName.length - b.firstName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (fileUrl) => <Image
      width={100}
      height={85}
      style={{borderRadius:50}}
      src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
    />
    },
    {
      key: 'name',
      dataIndex: 'name',
      title: 'Doctor Name',
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (name) => <strong>{name}</strong>
    },
    {
      title: 'OPD Timings',
      key: 'opdTimings',
      dataIndex: 'opdTimings',
      
      sorter: (a, b) => a.opdTimings.length - b.opdTimings.length,
      render: (opdTimings) => <div
        //onClick={()=>handleShowInfo} 
        style={{ cursor: 'pointer' }}><strong>{opdTimings}</strong></div>
    },
    {
      key: 'specialization',
      dataIndex: 'specialization',
      title: 'Specialization',
      sorter: (a, b) => a.specialization.length - b.specialization.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (specialization) => <strong>{specialization}</strong>
    },
    
    {
      key: 'mobileNumber',
      dataIndex: 'mobileNumber',
      title: 'Mobile Number',
      sorter: (a, b) => a.mobileNumber.length - b.mobileNumber.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (mobileNumber) => <strong>{mobileNumber}</strong>
    },
    {
      key: 'city',
      dataIndex: 'city',
      title: 'City',
      sorter: (a, b) => a.city.length - b.city.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (city) => <strong>{city}</strong>
    },
    {
      key: 'state',
      dataIndex: 'state',
      title: 'State',
      sorter: (a, b) => a.state.length - b.state.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (state) => <strong>{state}</strong>
    },
    
    {
      key: 'actions',
      title: 'Patients List',
      render: actions
    },

  ];

  

  const pagination = patients.length <= 10 ? false : {};
  console.log("patients all", patients);
  return (
    <div style={{height:'500px',overflowY:'scroll'}}>
      <Table
          pagination={pagination}
          className='accent-header'
          rowKey='id'
          dataSource={patients}
          columns={columns}
        />
      {/* {patients.length > 0 ?
        <Table
          pagination={pagination}
          className='accent-header'
          rowKey='id'
          dataSource={patients}
          columns={columns}
        //rowSelection={console.log('click')}
        />
        :

        <Row>
          <Col span={10}>
          </Col>
          <Col span={8}>
            <Space size="middle" style={{ marginTop: 10 }}>

              <Spin size="large" />
            </Space>
          </Col>
        </Row>

      } */}

      {/* <Modal
        visible={visibility}
        footer={null}
        onCancel={closeModal}
        title={<h3 className='title'>Add patient</h3>}
      >
        <PatientForm
          submitText='Update patient'
          onCancel={closeModal}
          onSubmit={onEditPatient}
          patient={patient}
        />
      </Modal> */}
    </div>
  );
};

export default PatientsTable;
