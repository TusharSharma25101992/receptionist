import React from 'react';

import { usePageData } from '../../../hooks/usePage';
import { usePatients } from '../../../hooks/usePatients';
import { Spin, Space, Row, Col, Divider,Tooltip,Input } from 'antd';
import DoctorForm from './DoctorForm';
import { history } from '../../../redux/store';
import {

  PlusCircleTwoTone,
} from '@ant-design/icons';
import { IPageData } from '../../../interfaces/page';

const pageData: IPageData = {
  title: 'Doctors',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Medicine',
      route: 'default-dashboard'
    },
    {
      title: 'Doctors'
    }
  ]
};

const DoctorsPage = () => {
  const { patients, editPatient, deletePatient } = usePatients();
  // usePageData(pageData);

  function addEmployee(){

    console.log("addEmployee");

    history.push({ pathname: `/vertical/form-elements`, state: {} });

  }

  return (
    <div>
    <Row>
          <Col span={3}>
            <h3 style={{ padding: 10, textAlign: 'left' }}>Doctors</h3>
          </Col>
          <Col span={8}>
            <Tooltip placement="top" title="Add Doctor">
              <PlusCircleTwoTone style={{ fontSize: 25, marginTop: 40, cursor: 'pointer' }} 
              onClick={addEmployee}
               />
            </Tooltip>

          </Col>
          <Col span={11}>

            <Input placeholder='Type to search' suffix={<span className='icofont icofont-search' />}
              style={{ width: '60%', float: 'right', marginTop: 40 }}
             // onChange={this.search}
            />

          </Col>

        </Row>
      <DoctorForm
        onDeletePatient={deletePatient}
        onEditPatient={editPatient}
        patients={patients}
      />
    </div>
  );
};

export default DoctorsPage;
