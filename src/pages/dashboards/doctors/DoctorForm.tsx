import React, { useState, useEffect } from 'react';
import { Avatar, Table, Button, Modal, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';

import { IPatient } from '../../../interfaces/patient';

import { history } from '../../../redux/store';
import PatientForm from '../../../layout/components/patients/PatientForm';

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import { Tooltip } from '@material-ui/core';

type Props = {
  patients: IPatient[];
  onEditPatient?: (patient: IPatient) => void;
  onDeletePatient?: (id: string) => void;
};

type PatientsImgProps = {
  img: string;
};

const PatientImg = ({ img }: PatientsImgProps) => {
  const isData = img.startsWith('data:image');
  const isWithPath = img.startsWith('http');

  if (isData || isWithPath) {
    return <Avatar size={40} src={img} />;
  }

  return <Avatar size={40} src={`${window.location.origin}/${img}`} />;
};

const DoctorForm = ({
  patients,
  onEditPatient = () => null,
  onDeletePatient = () => null
}: Props) => {

  const [deleteData, setDeleteData] = useState("");
  const [patient, setPatient] = useState(null);
  const [visibility, setVisibility] = useState(false);
  const [change, setChange] = useState(false);
  const [docList, setDoctorList] = useState([]);

  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [modalText, setModalText] = React.useState('Are you sure you want to delete this record?');


  useEffect(() => {



    getData()

  }, [])


 

  const getData = () => {

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user id", user.uid);

        let arr = [];
        await firebase.firestore()
          .collection("doctors").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setDoctorList(arr)
          });

      }
    })

  }

  console.log("docList", docList);

  const showModal = (data) => {
    setDeleteData("")
    console.log("data inside modal",data)
    setDeleteData(data.userID)
    setVisible(true);
  };

  const handleOk = async () => {
    console.log("deleteData",deleteData)
    // let uid = deleteData.userId;
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);

    // await firebase.firestore().collection("users").doc(userf.uid);
    await firebase.firestore().collection("doctors").doc(deleteData).delete().then(() => {
      console.log("Document successfully deleted!");
      setVisible(false);
      setConfirmLoading(false)
      // window.location.reload();
      history.push('/vertical/doctors')
      // getData();
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
    // setTimeout(() => {
    //   setVisible(false);
    //   setConfirmLoading(false);
    // }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  const handleEditPatient = (data) => {
    console.log('data',data)
    // history.push('/vertical/formElementsEdit');
    history.push({
      pathname: "/vertical/formElementsEdit",
      state: data,
    });

  }

  const closeModal = () => setVisibility(false);

  const handleShowInfo =(data)=>{

    console.log('data',data)
    // history.push('/vertical/formElementsEdit');
    history.push({
      pathname: "/vertical/docPatients",
      state: data,
    });


  }

  const actions = (data) => (
  
    // console.log("userId::",data)
    // let dat = data.userId;
   
  // return(
    
    <div className='buttons-list nowrap'>
      <Tooltip title="Show all Appointments">
      <Button shape='circle' onClick={()=>handleShowInfo(data)}>
        <span className='icofont-listine-dots' />
      </Button>
      </Tooltip>
      <Tooltip title="Edit">
      <Button
        onClick={()=>handleEditPatient(data)} 
        shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      </Tooltip>
      <Tooltip title="Delete">
      <Button
        onClick={()=>showModal(data)}
        shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
      </Tooltip>
      <Modal
        title=""
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>{modalText}</p>
      </Modal>
    </div>
  );
    // }

  const columns = [
    // {
    //   key: 'img',
    //   title: 'Photo',
    //   dataIndex: 'img',
    //   render: (img) => <PatientImg img={img} />
    // },
    // {
    //   key: 'hospitalName',
    //   dataIndex: 'hospitalName',
    //   title: 'Hospital/Clinic Name',
    //   sorter: (a, b) => a.hospitalName.length - b.hospitalName.length,
    //   // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //   render: (hospitalName) => <strong>{hospitalName}</strong>
    // },
    {
      key: 'firstName',
      dataIndex: 'firstName',
      title: 'Name',
      sorter: (a, b) => a.firstName.length - b.firstName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (firstName) => <strong>{firstName}</strong>
    },
    
    {
      key: 'mobileNumber',
      dataIndex: 'mobileNumber',
      title: 'Mobile Number',
      sorter: (a, b) => a.mobileNumber.length - b.mobileNumber.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (mobileNumber) => <strong>{mobileNumber}</strong>
    },
    {
      key: 'city',
      dataIndex: 'city',
      title: 'City',
      sorter: (a, b) => a.city.length - b.city.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (city) => <strong>{city}</strong>
    },
    {
      key: 'state',
      dataIndex: 'state',
      title: 'State',
      sorter: (a, b) => a.state.length - b.state.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (state) => <strong>{state}</strong>
    },
    {
      key: 'specialization',
      dataIndex: 'specialization',
      title: 'Specialization',
      sorter: (a, b) => a.specialization.length - b.specialization.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (specialization) => <strong>{specialization}</strong>
    },
    // {
    //   key: 'id',
    //   dataIndex: 'id',
    //   title: 'ID',
    //   sorter: (a, b) => (a.id > b.id ? 1 : -1),
    //   render: (id) => (
    //     <span className='nowrap' style={{ color: '#a5a5a5' }}>
    //       {id}
    //     </span>
    //   )
    // },
    // {
    //   key: 'age',
    //   dataIndex: 'age',
    //   title: 'Age',
    //   sorter: (a, b) => a.age - b.age,
    //   render: (age) => (
    //     <span className='nowrap' style={{ color: '#a5a5a5' }}>
    //       {age}
    //     </span>
    //   )
    // },
    // {
    //   key: 'address',
    //   dataIndex: 'address',
    //   title: 'Address',
    //   render: (address) => <span style={{ minWidth: 200, display: 'block' }}>{address}</span>
    // },
    // {
    //   key: 'number',
    //   dataIndex: 'number',
    //   title: 'Number',
    //   render: (phone) => (
    //     <span className='d-flex align-baseline nowrap' style={{ color: '#336cfb' }}>
    //       <span className='icofont icofont-ui-cell-phone mr-1' style={{ fontSize: 16 }} />
    //       {phone}
    //     </span>
    //   )
    // },
    // {
    //   key: 'visit',
    //   dataIndex: 'lastVisit',
    //   title: 'Last visit',
    //   render: (visit) => (
    //     <span className='nowrap' style={{ color: '#a5a5a5' }}>
    //       {visit}
    //     </span>
    //   )
    // },
    {
      key: 'status',
      dataIndex: 'status',
      title: 'Status',
      render: (status) => (
        // <Tag style={{ borderRadius: 20 }} color={status === 'Approved' ? '#b7ce63' : '#cec759'}>
        //   New
        // </Tag>
        <Tag style={{ borderRadius: 20 }} color={'#b7ce63'}>
          {status}
        </Tag>
      ),
      // sorter: (a, b) => (a.status > b.status ? 1 : -1)
    },
    {
      key: 'actions',
      title: 'Actions',
      render: actions
    }
  ];

  const pagination = patients.length <= 10 ? false : {};

  return (
    <>
      <Table
        pagination={pagination}
        className='accent-header'
        rowKey='id'
        dataSource={docList}
        columns={columns}
      />

      <Modal
        visible={visibility}
        footer={null}
        onCancel={closeModal}
        title={<h3 className='title'>Add patient</h3>}
      >
        <PatientForm
          submitText='Update patient'
          onCancel={closeModal}
          onSubmit={onEditPatient}
          patient={patient}
        />
      </Modal>
    </>
  );
};

export default DoctorForm;
