import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
// import MaskedInput from 'antd-mask-input';
import axios from "axios";
// import _ from 'underscore';
import Table from "@material-ui/core/Table";
// import DatePicker from "react-datepicker";
import { TextareaAutosize } from "@material-ui/core";
import moment from "moment";
// import "react-datepicker/dist/react-datepicker.css";
import DeleteForeverOutlinedIcon from "@material-ui/icons/DeleteForeverOutlined";
// import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// import Checkbox from '@material-ui/core/Checkbox';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import GetAppIcon from "@material-ui/icons/GetApp";
// import setting from '../../config/setting'
import { history } from '../../redux/store';
import {
  Alert,
  AutoComplete,
  // Button,
  Card,
  Checkbox,
  Input,
  Radio,
  Rate,
  Select,
  Switch,
  Tag,
  Form,
  DatePicker,
  Spin, Space, Row, Col, Divider, Tooltip
} from 'antd';

import { NavLink } from 'react-router-dom';
import { usePageData } from '../../hooks/usePage';
import { IPageData } from '../../interfaces/page';
import { BookOutlined, UserOutlined, MailOutlined } from '@ant-design/icons/lib';

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

import secondaryFireApp from '../../secondaryApp'

const FormItem = Form.Item;
const { Option } = Select;

// console.log("secondaryFireApp",secondaryFireApp)

const options = [
  { label: 'Checkbox 1', value: '1' },
  { label: 'Checkbox 2', value: '2' },
  { label: 'Checkbox 3', value: '3' }
];

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px'
};

const pageData: IPageData = {
  title: 'Form elements',
  fulFilled: true,
  breadcrumbs: [
    {
      title: 'Home',
      route: 'default-dashboard'
    },
    {
      title: 'UI Kit ',
      route: 'default-dashboard'
    },
    {
      title: 'Form elements'
    }
  ]
};

const FormElementsPage = () => {
  // usePageData(pageData);

  const [dataSource, setDataSource] = useState([]);

  const [typeVal, type] = useState("");
  const [firstNameVal, firstName] = useState("");
  const [lastNameVal, lastName] = useState("");
  const [emailAddressVal, emailAddress] = useState("");
  const [passwordVal, password] = useState("");
  // const [phoneVal, phone] = useState("");

  const [hospitalNameVal, hospitalName] = useState("");
  const [streetVal, street] = useState("");
  const [street2Val, street2] = useState("");
  const [stateVal, state] = useState("");
  const [cityVal, city] = useState("");
  const [zipVal, zip] = useState("");
  const [educationVal, education] = useState("");
  const [experienceVal, experience] = useState("");
  const [specializationVal, specialization] = useState("");
  const [opdTimeVal, opdTime] = useState("");
  const [opdFeeVal, opdFee] = useState("");
  const [opdTimingsVal, opdTimings] = useState("");
  const [mobileNumberVal, mobileNumber] = useState("");
  const [appointMobNumberVal, appointMobNumber] = useState("");
  const [statusVal, status] = useState("");
  const [change, setChange] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  const [changeNNNN, setChangeNNNN] = useState(false);
  const [hospitalList, setHospitalList] = useState([]);
  const [changeArr, setChangeArr] = useState(false);
  const [receptionistArr, setReceptionistArr] = useState([]);

  const [receptionistArrNew, setReceptionistArrNew] = useState([{
    id:'',
    firstName:'',
    lastName:'',
    hospitalId:'',
    mobileNumber:'',

  }]);
  
  const [hospitalArr, setHospitalArr] = useState([
    {
      id:'',
      name:'',
      receptionist:receptionistArrNew,
    }
  ]);
  // const [dataSource, setDataSource] = useState([]);

  const handleSearch = (value) =>
    setDataSource(!value ? [] : [value, value + value, value + value + value]);

  const [radioValue, setRadioValue] = useState(1);



  useEffect(() => {

    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user", user);
        let arr = [];
        await firebase.firestore()
          .collection("hospitals").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setHospitalList(arr)
          })
      }
    })

  }, [])

  console.log("hospitalList", hospitalList)
  const handleRadioChange = (e) => setRadioValue(e.target.value);

  const saveDoctorData = () => {

    console.log("state", hospitalArr)

    // firebase.auth().onAuthStateChanged(async (user) => {

    //   secondaryFireApp
    //     .auth()
    //     .createUserWithEmailAndPassword(emailAddressVal, (passwordVal.length > 0 ? passwordVal : "test1234"))
    //     .then(async (res) => {
    //       console.log("res", res)

    //       var userf = secondaryFireApp.auth().currentUser;
    //       console.log("userf", userf)

    //       let dataToInsertUser = {
    //         userID: userf.uid,
    //         role: "doctor",
    //         firstName: firstNameVal,
    //         lastName: lastNameVal,
    //         email: emailAddressVal,
    //         code: (passwordVal.length > 0 ? passwordVal : "test1234"),
    //         mobileNumber: mobileNumberVal,
    //         createdOn: new Date().toISOString(),
    //         createdBy: firebase.auth().currentUser.uid,
    //         createdByUserEmail: firebase.auth().currentUser.email,
    //       }

    //       let dataToInsertDoctor = {

    //         createdOn: new Date().toISOString(),
    //         createdBy: firebase.auth().currentUser.uid,
    //         createdByUserEmail: firebase.auth().currentUser.email,
    //         userID: res.user.uid,
    //         firstName: firstNameVal,
    //         lastName: lastNameVal,
    //         // email: emailAddressVal,
    //         // code: (passwordVal.length > 0 ? passwordVal : "test1234"),
    //         // type: typeVal,
    //         // phone:phoneVal,
    //         hospitalName: hospitalArr,
    //         street: streetVal,
    //         street2: street2Val,
    //         state: stateVal,
    //         city: cityVal,
    //         zip: zipVal,
    //         education: educationVal,
    //         experience: experienceVal,
    //         specialization: specializationVal,
    //         opdTime: opdTimeVal,
    //         opdFee: opdFeeVal,
    //         opdTimings: opdTimingsVal,
    //         mobileNumber: mobileNumberVal,
    //         appointMobNumber: appointMobNumberVal,
    //         status: statusVal,

    //       }
    //       console.log("submit state ", dataToInsertDoctor)
    //       await firebase.firestore().collection("users").doc(userf.uid)
    //         .set(dataToInsertUser)
    //         .then(ref => {
    //           console.log("current user dataToInsertDoctor", ref);

    //           // history.push('/vertical/doctors')
    //         })
    //       await firebase.firestore().collection("doctors").doc(userf.uid)
    //         .set(dataToInsertDoctor).then(ref => {
    //           console.log("current user dataToInsertDoctor", ref);

    //           // history.push('/vertical/doctors')
    //         })
    //       await history.push('/vertical/doctors')

    //     })

    //   secondaryFireApp.auth().signOut();

    // })
    // await firebase.firestore()
    //   .collection("WorkOrders").doc("WoItem")
    //   .collection(firebase.auth().currentUser.uid).doc(this.props.location.state.woId).update(editDataToInsert)

    // await firebase.firestore().collection("WorkOrders").doc("WoItem")
    //   .collection(firebase.auth().currentUser.uid).add(dataToInsert)
    //   .then(ref => {
    //     console.log("current user dataToInsert ref id", ref.id);
    //   })

  }

  const getHospitalReceptionist = async (hospitalId) => {

    console.log("hospitalId", hospitalId)

    let arrNew = [];
        await firebase.firestore()
        .collection("receptionist").where('hospitalId','==',hospitalId).get()
          .then((querySnapshot) =>{
            // console.log("querySnapshot",querySnapshot)
            querySnapshot.forEach( (doc)=> {

              console.log("doc receptionist", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arrNew.push(doc.data())

            });

          
            setChangeArr(true)
            setReceptionistArr(arrNew)

          })
  }

  console.log("receptionistArr", receptionistArr)


  return (
    <div>
      <h3 style={{ padding: 10, textAlign: 'left' }}>Add Doctor</h3>

      <Card>
        <Form layout='vertical'>
          
          {/* <div className='row'>
           
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Hospital/Clinic Name'>
              <Select
                  defaultValue=""
                  onChange={(e) => hospitalName(e)}
                >
                  {hospitalList.length > 0 && hospitalList.map((item, index) => (
                  <Option value={item.hospitalName}>{item.hospitalName}</Option>
                  ))}
                  
                </Select>
                
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Role'>
                <Select
                  defaultValue=""
                  onChange={(e) => type(e)}
                >
                  <Option value="hospital">Hospital</Option>
                  <Option value="clinic">Clinic</Option>
                </Select>
              </FormItem>
            </div>

          </div>
          <br /> */}
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Doctor First Name'>
                <Input
                  onChange={(e) => firstName(e.target.value)}
                  placeholder='Doctor First Name'
                  value={firstNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Doctor Last Name'>
                <Input

                  placeholder='Doctor Last Name'
                  onChange={(e) => lastName(e.target.value)}
                  value={lastNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>
          
          <h4 style={{ padding: 10, textAlign: 'left' }}>Add Address Information</h4>

          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Street'>
                <Input placeholder='Street'
                  onChange={(e) => street(e.target.value)}
                  value={streetVal}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Street 2'>
                <Input

                  placeholder='Street 2'
                  onChange={(e) => street2(e.target.value)}
                  value={street2Val}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />
          <div className='row'>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='State'>

                <Select
                  showSearch
                  defaultValue=""
                  //placeholder="Select Country"
                  optionFilterProp="children"
                  onChange={(e) => state(e)}
                  // onFocus={onFocus}
                  //onBlur={onBlur}
                  // onSearch={onSearchState}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >

                  {/* {state.length > 0 && state.map((state, index) => ( */}

                  <Option value="Haryana">Haryana</Option>


                  {/* ))} */}

                </Select>
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='City'>
                <Input
                  onChange={(e) => city(e.target.value)}
                  placeholder='City'
                  value={cityVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />
          <div className='row'>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='Zip Code'>
                <Input placeholder='Zip Code'
                  onChange={(e) => zip(e.target.value)}
                  value={zipVal}
                />
              </FormItem>
            </div>

          </div><br />
          <h4 style={{ padding: 10, textAlign: 'left' }}>Add Doctor Information</h4>
          <div className='row'>

            {/* <div className='col-md-6 col-sm-12'>
                <FormItem label='Date Of Birth'>
                  <DatePicker
                    style={{ width: '100%' }}
                    // onChange={(date, dateString) => onChangeDate(date, dateString)}
                  //onChange={onChangeDate} 
                  />
                </FormItem>
              </div> */}
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Education'>
                <Input

                  onChange={(e) => education(e.target.value)}
                  value={educationVal}
                //readOnly
                //disabled
                //defaultValue={age}
                // prefix={age}
                />
              </FormItem>
            </div>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='Experience'>
                <Input

                  onChange={(e) => experience(e.target.value)}
                  value={experienceVal}
                //readOnly
                //disabled
                //defaultValue={age}
                // prefix={age}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Specialization'>
                <Select defaultValue=""
                  onChange={(e) => specialization(e)}
                >
                  <Option value="Dental">Dental</Option>
                  <Option value="Ayurvedic">Ayurvedic</Option>
                  <Option value="ENT">ENT</Option>
                </Select>
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Average OPD Time'>
                <Select defaultValue=""
                  onChange={(e) => opdTime(e)}
                >
                  <Option value="5">5 mins</Option>
                  <Option value="10">10 mins</Option>
                  {/* <Option value="PHMSA-office">PHMSA-office</Option>
                  <Option value="PHMSA-field">PHMSA-field</Option> */}
                  {/* <Option value="Active">Active</Option>
      <Option value="Teminated">Teminated</Option> */}
                </Select>
              </FormItem>
            </div>
          </div>
          <br />
          <div className='row'>

            <div className='col-md-6 col-sm-6'>
              <FormItem label='OPD Fee'>
                <Input placeholder='OPD Fee'
                  onChange={(e) => opdFee(e.target.value)}
                  value={opdFeeVal}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='OPD Timings'>
                <Input

                  placeholder='OPD Timings'
                  onChange={(e) => opdTimings(e.target.value)}
                  value={opdTimingsVal}
                //pattern="[1-9]{1}[0-9]{9}"
                //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

          </div>


          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Mobile Number'>
                <Input

                  placeholder='Mobile Number'
                  onChange={(e) => mobileNumber(e.target.value)}
                  value={mobileNumberVal}
                //pattern="[1-9]{1}[0-9]{9}"
                //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='Appointment Phone Number'>
                <Input
                  onChange={(e) => appointMobNumber(e.target.value)}
                  placeholder='Appointment Phone Number'
                  value={appointMobNumberVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />

          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Email Address'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Email Address'
                  onChange={(e) => emailAddress(e.target.value)}
                  value={emailAddressVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Password'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Password'
                  onChange={(e) => password(e.target.value)}
                  value={passwordVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>

            </div>

          </div>

          <div className='row'>
            <div className='col-md-12 col-sm-12'>
              <FormItem label='Status'>
                <Select defaultValue=""
                  onChange={(e) => status(e)}
                >
                  <Option value="Activated">Activated</Option>
                  <Option value="Deactivated">Deactivated</Option>
                  {/* <Option value="other">Other</Option> */}
                </Select>
              </FormItem>
            </div>
          </div>
<br/>
<Grid container spacing={6}>
            <Grid item xs={10} sm={10}>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",
                  width: 220,
                }}
                onClick={() => {
                  setChangeN(false)
                  hospitalArr.push({
                    id:'',
                    name:'',
                    receptionist:[{ id:'',
                    firstName:'',
                    lastName:'',
                    hospitalId:'',
                    mobileNumber:'',
                }],
                  });
                  setChangeN(!changeN)
                  
                  console.log("hospitalArr", hospitalArr);
                }}
              >
                Add Hospital / Clinic
                      </Button>
            </Grid>
          </Grid>
          <br/>
          <TableContainer style={{ marginBottom: 20 }}>
            <Table aria-label="customized table" size="small">
              <TableHead>
                <TableRow style={{ backgroundColor: "#ebedef" }}>
                  <TableCell
                    style={{ border: "1px solid black" }}
                    align="center"
                  >
                    <b>Hospital/Clinic Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black",width:'20%' }}
                    align="center"
                  >
                    <b>Delete</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {hospitalArr.length > 0 &&
                          hospitalArr.map((data, idx) => (
                <TableRow key ={idx}>
                  <TableCell
                    style={{ border: "1px solid black",width:'50%' }}
                    align="center"
                  >
                    <Select
                      defaultValue={hospitalArr[idx].name}
                      onChange={(e) => 
                        {
                        console.log("hospitalArr change", JSON.parse(e));
                          
                        hospitalArr[idx].id = JSON.parse(e).userID;
                        hospitalArr[idx].name = JSON.parse(e).hospitalName;
                        setChangeN(!changeN)
                        getHospitalReceptionist(JSON.parse(e).userID)
                      }
                    }
                    >
                      {/* <option></option> */}
                      {hospitalList.length > 0 && hospitalList.map((item, index) => (
                        <Option 
                        selected={
                          item.hospitalName == hospitalArr[idx].name
                            ? true
                            : false
                        } 
                        value={JSON.stringify(item)}>{item.hospitalName}</Option>
                      ))}
                      {/* <Option value="clinic">Clinic</Option> */}
                    </Select>
                    <br/><br/>
                    <span style={{fontSize:13,fontWeight:700}}>Select Receptionist : </span>
                    <Select
                      defaultValue={receptionistArrNew[idx].firstName}
                      onChange={(e) => 
                        {
                        console.log("receptionistArr change", JSON.parse(e));
                          
                        receptionistArrNew[idx].id = JSON.parse(e).userID;
                        receptionistArrNew[idx].firstName = JSON.parse(e).firstName;
                        receptionistArrNew[idx].lastName = JSON.parse(e).lastName;
                        receptionistArrNew[idx].hospitalId = JSON.parse(e).hospitalId;
                        receptionistArrNew[idx].mobileNumber = JSON.parse(e).mobileNumber;
                        setChangeNNNN(!changeNNNN)
                      }
                    }
                    >
                      {/* <option></option> */}
                      {receptionistArr.length > 0 && receptionistArr.map((item, index) => (
                        <Option 
                        // selected={
                        //   item.firstName == receptionistArrNew[idx].firstName
                        //     ? true
                        //     : false
                        // } 
                        value={JSON.stringify(item)}>{item.firstName + " " + item.lastName}</Option>
                      ))}
                      {/* <Option value="clinic">Clinic</Option> */}
                    </Select>
                  </TableCell>
                  <TableCell style={{ border: "1px solid black" }}
                    align="center">
                  <IconButton
                                  style={{ padding: "0!important", height: 40 }}
                                  edge="end"
                                  aria-label="clear"
                                  onClick={() => {
                                    console.log("idx", idx);
                                    console.log("hospitalArr before", hospitalArr);
                                    hospitalArr.splice(idx, 1);
                                    // console.log("hospitalArr", hospitalArr);
                                    setHospitalArr(hospitalArr);
                                    console.log("hospitalArr", hospitalArr);
                                    setChangeNN(!changeNN);
                                  }}
                                >
                                  <DeleteForeverOutlinedIcon />
                                </IconButton>
                                </TableCell>
                </TableRow>
                          ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br/>
          <div className='row'>
            <div className='col-md-4 col-sm-4'>

            </div>
            <div className='col-md-5 col-sm-5'>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{ backgroundColor: "rgba(255,122,90,0.90)",
                color: "white",width: '70%' }}
                onClick={saveDoctorData}
              >
                Save
                      </Button>
              {/* <Button type='primary' style={{ width: '70%' }}
                onClick={saveDoctorData}
              >Save</Button> */}
            </div>
          </div>


        </Form>
      </Card>
    </div>
  );
};

export default FormElementsPage;
