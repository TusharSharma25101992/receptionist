import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
// import MaskedInput from 'antd-mask-input';
import axios from 'axios';
// import _ from 'underscore';
// import setting from '../../config/setting'
import Table from "@material-ui/core/Table";
// import DatePicker from "react-datepicker";
import { TextareaAutosize } from "@material-ui/core";
import moment from "moment";
// import "react-datepicker/dist/react-datepicker.css";
import DeleteForeverOutlinedIcon from "@material-ui/icons/DeleteForeverOutlined";
// import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// import Checkbox from '@material-ui/core/Checkbox';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import GetAppIcon from "@material-ui/icons/GetApp";
import { history } from '../../redux/store';
import {
  Alert,
  AutoComplete,
  // Button,
  Card,
  Checkbox,
  Input,
  Radio,
  Rate,
  Select,
  Switch,
  Tag,
  Form,
  DatePicker,
  Spin,
  Space,
  Row,
  Col,
  Divider,
  Tooltip
} from 'antd';

import { NavLink } from 'react-router-dom';
import { usePageData } from '../../hooks/usePage';
import { IPageData } from '../../interfaces/page';
import { BookOutlined, UserOutlined, MailOutlined } from '@ant-design/icons/lib';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

const FormItem = Form.Item;
const { Option } = Select;

const options = [
  { label: 'Checkbox 1', value: '1' },
  { label: 'Checkbox 2', value: '2' },
  { label: 'Checkbox 3', value: '3' }
];

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px'
};

// const pageData: IPageData = {
//   title: 'Form elements',
//   fulFilled: true,
//   breadcrumbs: [
//     {
//       title: 'Home',
//       route: 'default-dashboard'
//     },
//     {
//       title: 'UI Kit ',
//       route: 'default-dashboard'
//     },
//     {
//       title: 'Form elements'
//     }
//   ]
// };

const FormElementsEdit = () => {
  const location = useLocation();
  let data = location.state;
  // usePageData(pageData);
  console.log('props data', data);

  const [dataSource, setDataSource] = useState([]);

  const [typeVal, type] = useState(data.type);
  const [firstNameVal, firstName] = useState(data.firstName);
  const [lastNameVal, lastName] = useState(data.lastName);
  const [emailAddressVal, emailAddress] = useState(data.email);
  const [passwordVal, password] = useState(data.code);
  // const [phoneVal, phone] = useState("");

  const [hospitalNameVal, hospitalName] = useState(data.hospitalName);
  const [streetVal, street] = useState(data.street);
  const [street2Val, street2] = useState(data.street2);
  const [stateVal, state] = useState(data.state);
  const [cityVal, city] = useState(data.city);
  const [zipVal, zip] = useState(data.zip);
  const [educationVal, education] = useState(data.education);
  const [experienceVal, experience] = useState(data.experience);
  const [specializationVal, specialization] = useState(data.specialization);
  const [opdTimeVal, opdTime] = useState(data.opdTime);
  const [opdFeeVal, opdFee] = useState(data.opdFee);
  const [opdTimingsVal, opdTimings] = useState(data.opdTimings);
  const [mobileNumberVal, mobileNumber] = useState(data.mobileNumber);
  const [appointMobNumberVal, appointMobNumber] = useState(data.appointMobNumber);
  const [statusVal, status] = useState(data.status);
  const [change, setChange] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  const [hospitalList, setHospitalList] = useState([]);
  const [hospitalArr, setHospitalArr] = useState(data.hospitalName);

  // const [dataSource, setDataSource] = useState([]);

  const handleSearch = (value) =>
    setDataSource(!value ? [] : [value, value + value, value + value + value]);

  const [radioValue, setRadioValue] = useState(1);

  useEffect(() => {

    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user", user);
        let arr = [];
        await firebase.firestore()
          .collection("hospitals").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setHospitalList(arr)
          })
      }
    })

  }, [])

  console.log("hospitalList", hospitalList)

  const handleRadioChange = (e) => setRadioValue(e.target.value);

  const updateDoctorData = async () => {
    console.log('state', firstNameVal, lastNameVal, emailAddressVal, passwordVal);
    // var user = firebase.auth().currentUser;
    // firebase.auth().onAuthStateChanged(async (user) => {

    // firebase
    //   .auth()
    //   .createUserWithEmailAndPassword(emailAddressVal, (passwordVal.length > 0 ? passwordVal : "test1234"))
    //   .then(async (res) => {
    //     console.log("res",res)

    //     var userf = firebase.auth().currentUser;
    //     console.log("userf",userf)

    let dataToUpdate = {
      // createdBy: firebase.auth().currentUser.uid,
      //createdByUserEmail: firebase.auth().currentUser.email,
      //userId: res.user.uid,
      firstName: firstNameVal,
      lastName: lastNameVal,
      // email: emailAddressVal,
      // code: passwordVal.length > 0 ? passwordVal : 'test1234',
      // type: typeVal,
      hospitalName: hospitalArr,
      street: streetVal,
      street2: street2Val,
      state: stateVal,
      city: cityVal,
      zip: zipVal,
      education: educationVal,
      experience: experienceVal,
      specialization: specializationVal,
      opdTime: opdTimeVal,
      opdFee: opdFeeVal,
      opdTimings: opdTimingsVal,
      mobileNumber: mobileNumberVal,
      appointMobNumber: appointMobNumberVal,
      status: statusVal,
      updated_on: new Date().toISOString()
    };
    console.log('submit state ', dataToUpdate);

    await firebase
      .firestore()
      .collection('doctors')
      .doc(data.userID)
      .update(dataToUpdate)
      .then((ref) => {
        console.log('current user updated!');
        history.push('/vertical/doctors');
      });

    // await firebase.firestore().collection("users").doc(userf.uid)
    // .set(dataToUpdate)
    // .then(ref => {
    //       console.log("current user dataToUpdate", ref);
    //       history.push('/vertical/doctors')
    //     })

    //   })

    // await firebase.firestore()
    //   .collection("WorkOrders").doc("WoItem")
    //   .collection(firebase.auth().currentUser.uid).doc(this.props.location.state.woId).update(editDataToInsert)

    // await firebase.firestore().collection("WorkOrders").doc("WoItem")
    //   .collection(firebase.auth().currentUser.uid).add(dataToInsert)
    //   .then(ref => {
    //     console.log("current user dataToInsert ref id", ref.id);
    //   })
  };

  return (
    <div>
      <h3 style={{ padding: 10, textAlign: 'left' }}>Edit Doctor Information</h3>

      <Card>
        <Form layout='vertical'>
        
          {/* <div className='row'>
            
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Hospital/Clinic Name'>
                <Input
                  onChange={(e) => hospitalName(e.target.value)}
                  placeholder='Hospital/Clinic Name'
                  value={hospitalNameVal}
                 
                />
              </FormItem>
            
            </div>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Role'>
                <Select defaultValue={typeVal} onChange={(e) => type(e)}>
                  <Option value='hospital'>Hospital</Option>
                  <Option value='clinic'>Clinic</Option>
                </Select>
              </FormItem>
            </div>
          </div>
          <br /> */}
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='First Name'>
                <Input
                  onChange={(e) => firstName(e.target.value)}
                  placeholder='First Name'
                  value={firstNameVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Last Name'>
                <Input
                  placeholder='Last Name'
                  onChange={(e) => lastName(e.target.value)}
                  value={lastNameVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <h4 style={{ padding: 10, textAlign: 'left' }}>Edit Address Information</h4>

          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Street'>
                <Input
                  placeholder='Street'
                  onChange={(e) => street(e.target.value)}
                  value={streetVal}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Street 2'>
                <Input
                  placeholder='Street 2'
                  onChange={(e) => street2(e.target.value)}
                  value={street2Val}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='State'>
                <Select
                  showSearch
                  defaultValue={stateVal}
                  //placeholder="Select Country"
                  optionFilterProp='children'
                  onChange={(e) => state(e)}
                  // onFocus={onFocus}
                  //onBlur={onBlur}
                  // onSearch={onSearchState}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {/* {state.length > 0 && state.map((state, index) => ( */}

                  <Option value='Haryana'>Haryana</Option>

                  {/* ))} */}
                </Select>
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='City'>
                <Input
                  onChange={(e) => city(e.target.value)}
                  placeholder='City'
                  value={cityVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Zip Code'>
                <Input
                  placeholder='Zip Code'
                  onChange={(e) => zip(e.target.value)}
                  value={zipVal}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <h4 style={{ padding: 10, textAlign: 'left' }}>Edit Doctor Information</h4>
          <div className='row'>
            {/* <div className='col-md-6 col-sm-12'>
                <FormItem label='Date Of Birth'>
                  <DatePicker
                    style={{ width: '100%' }}
                    // onChange={(date, dateString) => onChangeDate(date, dateString)}
                  //onChange={onChangeDate} 
                  />
                </FormItem>
              </div> */}
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Education'>
                <Input
                  onChange={(e) => education(e.target.value)}
                  value={educationVal}
                  //readOnly
                  //disabled
                  //defaultValue={age}
                  // prefix={age}
                />
              </FormItem>
            </div>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='Experience'>
                <Input
                  onChange={(e) => experience(e.target.value)}
                  value={experienceVal}
                  //readOnly
                  //disabled
                  //defaultValue={age}
                  // prefix={age}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Specialization'>
                <Select defaultValue={specializationVal} onChange={(e) => specialization(e)}>
                  <Option value='Dental'>Dental</Option>
                  <Option value='Ayurvedic'>Ayurvedic</Option>
                  <Option value='ENT'>ENT</Option>
                </Select>
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Average OPD Time'>
                <Select defaultValue={opdTimeVal} onChange={(e) => opdTime(e)}>
                  <Option value='5'>5 mins</Option>
                  <Option value='10'>10 mins</Option>
                  {/* <Option value="PHMSA-office">PHMSA-office</Option>
                  <Option value="PHMSA-field">PHMSA-field</Option> */}
                  {/* <Option value="Active">Active</Option>
      <Option value="Teminated">Teminated</Option> */}
                </Select>
              </FormItem>
            </div>
          </div>
          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='OPD Fee'>
                <Input
                  placeholder='OPD Fee'
                  onChange={(e) => opdFee(e.target.value)}
                  value={opdFeeVal}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='OPD Timings'>
                <Input
                  placeholder='OPD Timings'
                  onChange={(e) => opdTimings(e.target.value)}
                  value={opdTimingsVal}
                  //pattern="[1-9]{1}[0-9]{9}"
                  //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />
          <div className='row'>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Mobile Number'>
                <Input
                  placeholder='Mobile Number'
                  onChange={(e) => mobileNumber(e.target.value)}
                  value={mobileNumberVal}
                  //pattern="[1-9]{1}[0-9]{9}"
                  //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

            <div className='col-md-6 col-sm-12'>
              <FormItem label='Appointment Phone Number'>
                <Input
                  onChange={(e) => appointMobNumber(e.target.value)}
                  placeholder='Appointment Phone Number'
                  value={appointMobNumberVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>

          <br />

          {/* <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Email Address'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Email Address'
                  onChange={(e) => emailAddress(e.target.value)}
                  value={emailAddressVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Password'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Password'
                  onChange={(e) => password(e.target.value)}
                  value={passwordVal}
                  //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div> */}

          <div className='row'>
            <div className='col-md-12 col-sm-12'>
              <FormItem label='Status'>
                <Select defaultValue={statusVal} onChange={(e) => status(e)}>
                  <Option value='Activated'>Activated</Option>
                  <Option value='Deactivated'>Deactivated</Option>
                  {/* <Option value="other">Other</Option> */}
                </Select>
              </FormItem>
            </div>
          </div>
          <br/>
          <Grid container spacing={6}>
            <Grid item xs={10} sm={10}>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",
                  width: 220,
                }}
                onClick={() => {
                  setChangeN(false)
                  hospitalArr.push({
                    id:'',
                    name:''
                  });
                  setChangeN(!changeN)
                  
                  console.log("hospitalArr", hospitalArr);
                }}
              >
                Add Hospital / Clinic
                      </Button>
            </Grid>
          </Grid>
          <br/>
          <TableContainer style={{ marginBottom: 20 }}>
            <Table aria-label="customized table" size="small">
              <TableHead>
                <TableRow style={{ backgroundColor: "#ebedef" }}>
                  <TableCell
                    style={{ border: "1px solid black" }}
                    align="center"
                  >
                    <b>Hospital/Clinic Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black",width:'20%' }}
                    align="center"
                  >
                    <b>Delete</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {hospitalArr.length > 0 &&
                          hospitalArr.map((data, idx) => (
                <TableRow key ={idx}>
                  <TableCell
                    style={{ border: "1px solid black",width:'50%' }}
                    align="center"
                  >
                    <Select
                      defaultValue={data.name}
                      onChange={(e) => 
                        {
                          
                        hospitalArr[idx].id = JSON.parse(e).userID;
                        hospitalArr[idx].name = JSON.parse(e).hospitalName;
                        setChangeN(!changeN)
                        console.log("hospitalArr", hospitalArr);
                      }
                    }
                    >
                      {/* <option></option> */}
                      {/* <Option value="">{data.name}</Option> */}
                      {hospitalList.length > 0 && hospitalList.map((item, index) => (
                        <Option value={JSON.stringify(item)}>{item.hospitalName}</Option>
                      ))}
                      {/* <Option value="clinic">Clinic</Option> */}
                    </Select>
                  </TableCell>
                  <TableCell style={{ border: "1px solid black" }}
                    align="center">
                  <IconButton
                                  style={{ padding: "0!important", height: 40 }}
                                  edge="end"
                                  aria-label="clear"
                                  onClick={() => {
                                    console.log("idx", idx);
                                    hospitalArr.splice(idx, 1);
                                    setHospitalArr(hospitalArr);
                                    setChangeNN(!changeNN);
                                    console.log("hospitalArr", hospitalArr);
                                  }}
                                >
                                  <DeleteForeverOutlinedIcon />
                                </IconButton>
                                </TableCell>
                </TableRow>
                          ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br/>
          
          <div className='row'>
            <div className='col-md-4 col-sm-4'></div>
            <div className='col-md-5 col-sm-5'>
              <Button variant="contained"  style={{ backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",width: '70%' }} onClick={updateDoctorData}>
                Update
              </Button>
            </div>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default FormElementsEdit;
