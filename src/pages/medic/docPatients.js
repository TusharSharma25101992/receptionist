import React, { useState, useEffect } from 'react';
import { Avatar, Table, Button, Modal, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { Spin, Space, Row, Col, Divider, Tooltip, Input } from 'antd';
// import { IPatient } from '../../../interfaces/patient';
import { useLocation } from 'react-router-dom';
import moment from 'moment';
import _ from 'underscore';
import { DatePicker } from 'antd';
import { PlusCircleTwoTone } from '@ant-design/icons';
import { history } from '../../redux/store';
// import PatientForm from '../../../layout/components/patients/PatientForm';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

// type Props = {
//   patients: IPatient[];
//   onEditPatient?: (patient: IPatient) => void;
//   onDeletePatient?: (id: string) => void;
// };

// type PatientsImgProps = {
//   img: string;
// };

// const PatientImg = ({ img }: PatientsImgProps) => {
//   const isData = img.startsWith('data:image');
//   const isWithPath = img.startsWith('http');

//   if (isData || isWithPath) {
//     return <Avatar size={40} src={img} />;
//   }

//   return <Avatar size={40} src={`${window.location.origin}/${img}`} />;
// };

const DocPatients = () => {
  const location = useLocation();
  let data = location.state;
  // usePageData(pageData);
  console.log('props data', data);

  const [deleteData, setDeleteData] = useState('');
  const [patient, setPatient] = useState(null);
  const [visibility, setVisibility] = useState(false);
  const [change, setChange] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeData, setChangeData] = useState(false);
  const [docList, setDoctorList] = useState([]);
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));

  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [modalText, setModalText] = React.useState('Are you sure you want to delete this record?');

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log('user id', user.uid);

        let arr = [];
        await Promise.all(data.hospitalName.map(async (hospId, idx) => {
        // new Promise.all(_.each(data.hospitalName, (hospId) => {
          await firebase
            .firestore()
            .collection('patients')
            .doc(date)
            .collection(hospId.id)
            .where('DoctorId', '==', data.userID)
            .get()
            .then( (querySnapshot) =>{
              querySnapshot.forEach( (doc)=> {
                console.log('doc', doc.data().createdOn.toDate());

                arr.push(doc.data());

                
              });
              // setChangeN(true)
            });
        }))
        console.log("arr",arr);
        setDoctorList(arr);
                
                setChange(true);
      }
    });
  };

  console.log('docList', docList);

  const showModal = (data) => {
    setDeleteData('');
    console.log('data inside modal', data);
    setDeleteData(data.userID);
    setVisible(true);
  };

  const handleOk = async () => {
    console.log('deleteData', deleteData);
    // let uid = deleteData.userId;
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);

    // await firebase.firestore().collection("users").doc(userf.uid);
    await firebase
      .firestore()
      .collection('hospitals')
      .doc(deleteData)
      .delete()
      .then(() => {
        console.log('Document successfully deleted!');
        setVisible(false);
        setConfirmLoading(false);
        // window.location.reload();
        history.push('/vertical/hospitalList');
        // getData();
      })
      .catch((error) => {
        console.error('Error removing document: ', error);
      });
    // setTimeout(() => {
    //   setVisible(false);
    //   setConfirmLoading(false);
    // }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  const handleEditPatient = (data) => {
    console.log('data', data);
    // history.push('/vertical/formElementsEdit');
    history.push({
      pathname: '/vertical/hospitalEdit',
      state: data
    });
  };

  const closeModal = () => setVisibility(false);

  //   const handleShowInfo = () => history.push('/vertical/patient-profile');
  //   const handleDeletePatient = (id) => onDeletePatient(id);

  const actions = (data) => (
    <div className='buttons-list nowrap'>
      {/* <Button shape='circle' onClick={handleShowInfo}>
        <span className='icofont icofont-external-link' />
      </Button> */}
      <Button onClick={() => handleEditPatient(data)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={() => showModal(data)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
      <Modal
        title=''
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>{modalText}</p>
      </Modal>
    </div>
  );

  const columns = [
    // {
    //   key: 'img',
    //   title: 'Photo',
    //   dataIndex: 'img',
    //   render: (img) => <PatientImg img={img} />
    // },
    // {
    //     key: 'userID',
    //     dataIndex: 'userID',
    //     title: 'User ID',
    //     sorter: (a, b) => a.userID.length - b.userID.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (userID) => <strong>{userID}</strong>
    //   },
    // {
    //   key: 'patientId',
    //   dataIndex: 'patientId',
    //   title: 'Patient Id',
    //   sorter: (a, b) => a.patientId.length - b.patientId.length,
    //   // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //   render: (patientId) => <strong>{patientId}</strong>
    // },
    {
      key: 'AppointmentID',
      dataIndex: 'AppointmentID',
      title: 'Appointment ID',
      sorter: (a, b) => a.AppointmentID.length - b.AppointmentID.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (AppointmentID) => <strong>{AppointmentID}</strong>
    },
    {
      key: 'patientFirstName',
      dataIndex: 'patientFirstName',
      title: 'Patient First Name',
      sorter: (a, b) => a.patientFirstName.length - b.patientFirstName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (patientFirstName) => <strong>{patientFirstName}</strong>
    },
    {
      key: 'patientLastName',
      dataIndex: 'patientLastName',
      title: 'Patient Last Name',
      sorter: (a, b) => a.patientLastName.length - b.patientLastName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (patientLastName) => <strong>{patientLastName}</strong>
    },
    {
      key: 'phoneAge',
      dataIndex: 'phoneAge',
      title: 'Age',
      sorter: (a, b) => a.phoneAge.length - b.phoneAge.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (phoneAge) => <strong>{phoneAge}</strong>
    },
    {
      key: 'phoneNumber',
      dataIndex: 'phoneNumber',
      title: 'Phone Number',
      sorter: (a, b) => a.phoneNumber.length - b.phoneNumber.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (phoneNumber) => <strong>{phoneNumber}</strong>
    },
    {
      //key: 'phoneNumber',
      //dataIndex: 'phoneNumber',
      title: 'Status',
      //sorter: (a, b) => a.phoneNumber.length - b.phoneNumber.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: () => <strong>New</strong>
    },
    {
      key: 'createdOn',
      dataIndex: 'createdOn',
      title: 'Created On',
      sorter: (a, b) => a.createdOn.length - b.createdOn.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (createdOn) => <strong>{moment(createdOn.toDate()).format('DD/MM/YYYY HH:ss')}</strong>
    }
    //   {
    //     key: 'code',
    //     dataIndex: 'code',
    //     title: 'Password',
    //     sorter: (a, b) => a.code.length - b.code.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (code) => <strong>{code}</strong>
    //   },

    // {
    //   key: 'actions',
    //   title: 'Actions',
    //   render: actions
    // }
  ];

  const pagination = docList.length <= 10 ? false : {};

  // function addEmployee(){

  //   console.log("addEmployee");

  //   history.push({ pathname: `/vertical/departments`, state: {} });

  // }

  const onChangeDate = (date, dateString)=> {
    console.log('date', date, dateString);
    setDate(dateString);
    setChangeData(true);
    // getData();
  }

  const search = () =>{
    getData();

  }

  return (
    <>
      <Row>
        <Col span={14}>
          <h3 style={{ padding: 10, textAlign: 'left' }}>
            All Patient Appointments ({data && data.firstName + ' ' + data.lastName})
          </h3>
        </Col>
        {/* <Col span={7}> */}
        {/* <Tooltip placement="top" title="Add Hospital / Clinic">
              <PlusCircleTwoTone style={{ fontSize: 25, marginTop: 40, cursor: 'pointer' }} 
              onClick={addEmployee}
               />
            </Tooltip> */}

        {/* </Col> */}
        <Col span={4}>
          <Space direction='' style={{ float: 'left', marginTop: 40 }}>
            <DatePicker onChange={onChangeDate} />
          </Space>

          {/* <Input placeholder='Type to search' suffix={<span className='icofont icofont-search' />}
              style={{ width: '60%', float: 'right', marginTop: 40 }}
             
            /> */}
        </Col>
        <Col span={4}>
        <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgb(64 44 40 / 90%)",height:30,
                  color: "white", width: '45%',float: 'left', marginTop: 40
                }}
                onClick={search}
              >
                Search
                      </Button>
        </Col>
      </Row>
      <Table
        pagination={pagination}
        className='accent-header'
        rowKey='id'
        dataSource={docList}
        columns={columns}
      />

      {/* <Modal
        visible={visibility}
        footer={null}
        onCancel={closeModal}
        title={<h3 className='title'>Add patient</h3>}
      >
        <PatientForm
          submitText='Update patient'
          onCancel={closeModal}
          onSubmit={onEditPatient}
          patient={patient}
        />
      </Modal> */}
    </>
  );
};

export default DocPatients;
