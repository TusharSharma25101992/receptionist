import React, { useState, useEffect } from 'react';
import { Avatar, Table, Button, Modal, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { Spin, Space, Row, Col, Divider,Tooltip,Input } from 'antd';
// import { IPatient } from '../../../interfaces/patient';
import moment from 'moment';
import {

    PlusCircleTwoTone,
  } from '@ant-design/icons';
import { history } from '../../../redux/store';
// import PatientForm from '../../../layout/components/patients/PatientForm';

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

// type Props = {
//   patients: IPatient[];
//   onEditPatient?: (patient: IPatient) => void;
//   onDeletePatient?: (id: string) => void;
// };

// type PatientsImgProps = {
//   img: string;
// };

// const PatientImg = ({ img }: PatientsImgProps) => {
//   const isData = img.startsWith('data:image');
//   const isWithPath = img.startsWith('http');

//   if (isData || isWithPath) {
//     return <Avatar size={40} src={img} />;
//   }

//   return <Avatar size={40} src={`${window.location.origin}/${img}`} />;
// };

const AdsList = () => {

  const [deleteData, setDeleteData] = useState("");
  const [patient, setPatient] = useState(null);
  const [visibility, setVisibility] = useState(false);
  const [change, setChange] = useState(false);
  const [docList, setDoctorList] = useState([]);

  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [modalText, setModalText] = React.useState('Are you sure you want to delete this record?');


  useEffect(() => {

    getData()

  }, [])

  const getData = () => {

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user id", user.uid);

        let arr = [];
        await firebase.firestore()
          .collection("customAds").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push({...doc.data(),id:doc.id})

            });
            setChange(true)
            setDoctorList(arr)
          });

      }
    })

  }

  console.log("docList", docList);

  const showModal = (data) => {
    setDeleteData("")
    console.log("data inside modal",data)
    setDeleteData(data.id)
    setVisible(true);
  };

  const handleOk = async () => {
    console.log("deleteData",deleteData)
    // let uid = deleteData.userId;
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);

    // await firebase.firestore().collection("users").doc(userf.uid);
    await firebase.firestore().collection("customAds").doc(deleteData).delete().then(() => {
      console.log("Document successfully deleted!");
      setVisible(false);
      setConfirmLoading(false)
      // window.location.reload();
      history.push('/vertical/adsList')
      // getData();
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
    // setTimeout(() => {
    //   setVisible(false);
    //   setConfirmLoading(false);
    // }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  const handleEditPatient = (data) => {
    console.log('data',data)
    // history.push('/vertical/formElementsEdit');
    history.push({
      pathname: "/vertical/adsEdit",
      state: data,
    });

  }

 

  const closeModal = () => setVisibility(false);

//   const handleShowInfo = () => history.push('/vertical/patient-profile');
//   const handleDeletePatient = (id) => onDeletePatient(id);


  const actions = (data) => (
   
    <div className='buttons-list nowrap'>
      {/* <Button shape='circle' 
      onClick={()=>handleShowInfo(data)}
      >
        <span className='icofont icofont-external-link' />
      </Button> */}
      <Button
        onClick={()=>handleEditPatient(data)} 
        shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button
        onClick={()=>showModal(data)}
        shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
      <Modal
        title=""
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>{modalText}</p>
      </Modal>
    </div>
  );
    

  const columns = [
    // {
    //   key: 'img',
    //   title: 'Photo',
    //   dataIndex: 'img',
    //   render: (img) => <PatientImg img={img} />
    // },
    // {
    //     key: 'userID',
    //     dataIndex: 'userID',
    //     title: 'User ID',
    //     sorter: (a, b) => a.userID.length - b.userID.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (userID) => <strong>{userID}</strong>
    //   },
    {
      key: 'specialization',
      dataIndex: 'specialization',
      title: 'Specialization',
      sorter: (a, b) => a.specialization.length - b.specialization.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (specialization) => <strong>{specialization}</strong>
    },
    {
      key: 'city',
      dataIndex: 'city',
      title: 'City',
      sorter: (a, b) => a.city.length - b.city.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (city) => <strong>{city}</strong>
    },
    
    //   {
    //     key: 'code',
    //     dataIndex: 'code',
    //     title: 'Password',
    //     sorter: (a, b) => a.code.length - b.code.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (code) => <strong>{code}</strong>
    //   },
      {
      key: 'createdOn',
      dataIndex: 'createdOn',
      title: 'Created On',
      sorter: (a, b) => a.createdOn.length - b.createdOn.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (createdOn) => <strong>{moment(createdOn).format("DD/MM/YYYY")}</strong>
    },
    
    {
      key: 'actions',
      title: 'Actions',
      render: actions
    }
  ];

  const pagination = docList.length <= 10 ? false : {};

  function addEmployee(){

    console.log("addEmployee");

    history.push({ pathname: `/vertical/adsAdd`, state: {} });

  }

  return (
    <>
    <Row>
          <Col span={14}>
            <h3 style={{ padding: 10, textAlign: 'left' }}>Custom Ads List(for Doctor TV/Patient Tracking module)</h3>
          </Col>
          <Col span={4}>
            <Tooltip placement="top" title="Add Hospital / Clinic">
              <PlusCircleTwoTone style={{ fontSize: 25, marginTop: 40, cursor: 'pointer' }} 
              onClick={addEmployee}
               />
            </Tooltip>

          </Col>
          <Col span={6}>

            <Input placeholder='Type to search' suffix={<span className='icofont icofont-search' />}
              style={{ width: '60%', float: 'right', marginTop: 40 }}
             // onChange={this.search}
            />

          </Col>

        </Row>
      <Table
        pagination={pagination}
        className='accent-header'
        rowKey='id'
        dataSource={docList}
        columns={columns}
      />

      {/* <Modal
        visible={visibility}
        footer={null}
        onCancel={closeModal}
        title={<h3 className='title'>Add patient</h3>}
      >
        <PatientForm
          submitText='Update patient'
          onCancel={closeModal}
          onSubmit={onEditPatient}
          patient={patient}
        />
      </Modal> */}
    </>
  );
};

export default AdsList;
