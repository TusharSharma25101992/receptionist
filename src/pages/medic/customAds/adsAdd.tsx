import React, { useState, useEffect } from 'react';
import { IPageData } from '../../../interfaces/page';
import { history } from '../../../redux/store';
import Department from '../../../layout/components/department/Department';
import Table from "@material-ui/core/Table";
// import DatePicker from "react-datepicker";
import { TextareaAutosize } from "@material-ui/core";
import moment from "moment";
import _ from 'underscore';
// import "react-datepicker/dist/react-datepicker.css";
import DeleteForeverOutlinedIcon from "@material-ui/icons/DeleteForeverOutlined";
// import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// import Checkbox from '@material-ui/core/Checkbox';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import GetAppIcon from "@material-ui/icons/GetApp";
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IDepartment } from '../../../interfaces/patient';
import {
  Alert,
  AutoComplete,
  // Button,
  Card,
  Checkbox,
  Input,
  Radio,
  Rate,
  Select,
  Switch,
  Tag,
  Form,
  DatePicker,
  Spin, Space, Row, Col, Divider, Tooltip
} from 'antd';
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import secondaryFireApp from '../../../secondaryApp'


import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';

const { Dragger } = Upload;


const FormItem = Form.Item;
const { Option } = Select;
// const firebaseConfig = {
//   apiKey: "AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw",
//   authDomain: "opd-check-com.firebaseapp.com",
//   projectId: "opd-check-com",
//   storageBucket: "opd-check-com.appspot.com",
//   messagingSenderId: "473581330102",
//   appId: "1:473581330102:web:1415eceae267ee888bd017",
//   measurementId: "G-HL6YYGDVB1"
// };
// const secondaryFireApp = firebase.initializeApp(firebaseConfig, "secondaryFireApp");
const pageData: IPageData = {
  title: 'Departments',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Medicine',
      route: 'default-dashboard'
    },
    {
      title: 'Departments'
    }
  ]
};

const AdsAdd = () => {
  const [cityVal, city] = useState("");
  const [specializationVal, specialization] = useState("");
  const [infoListVal, infoList] = useState([]);
  const [textVal, text] = useState("");
  // const [lastNameVal, lastName] = useState("");
  // const [emailAddressVal, emailAddress] = useState("");
  // const [passwordVal, password] = useState("");
  // const [mobileNumberVal, mobileNumber] = useState("");

  const [change, setChange] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  // const [doctorList, setDoctorList] = useState([]);
  // const [doctorArr, setDoctorArr] = useState([
  //   {
  //     id: '',
  //     name: ''
  //   }
  // ]);
  const [departments] = useFetchPageData<IDepartment[]>('./data/departments.json', []);
  // usePageData(pageData);


  // const props = {
  //   name: 'file',
  //   multiple: true,
  //   action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  //   onChange(info) {
  //     console.log("info",info)
  //     const { status } = info.file;
  //     //infoListVal.push(info.file)
  //     if (status !== 'uploading') {
  //       console.log("info file",info.file);
  //       console.log("info fileList",info.fileList);
  //       infoList(info.fileList)
  //       setChange(true)
  //     }
  //     if (status === 'done') {
  //       message.success(`${info.file.name} file uploaded successfully.`);
  //     } else if (status === 'error') {
  //       message.error(`${info.file.name} file upload failed.`);
  //     }
  //   },
  //   onDrop(e) {
  //     console.log('Dropped files', e.dataTransfer.files);
  //   },
  // };

  const depClass = (i, length) => {
    if (i === length - 1) {
      return 'mb-0';
    }

    if (i > length - 4) {
      return 'mb-md-0';
    }

    return '';
  };

  useEffect(() => {

    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user", user);
        // let arr = [];
        // await firebase.firestore()
        //   .collection("doctors").get().then(function (querySnapshot) {
        //     querySnapshot.forEach(function (doc) {

        //       console.log("doc", doc.data());
        //       // doc.data() is never undefined for query doc snapshots

        //       arr.push(doc.data())

        //     });
        //     setChange(true)
        //     setDoctorList(arr)
        //   })
      }
    })

  }, [])

  // console.log("doctorList", doctorList)

  const saveHospitalName = async () => {

    console.log("ads city", cityVal)
    console.log("ads spec", specializationVal)
    console.log("ads file", infoListVal)
    console.log("ads text", textVal)

  
      //console.log("event",event.target.files[0]);
      if (infoListVal && infoListVal[0]) {
        let reader = new FileReader();
        reader.onload = (e) => {
          // this.setState({ image: e.target.result, scanning: true });
  
        };
        reader.readAsDataURL(infoListVal[0]);
  
      }
  
      const image = infoListVal[0];
      // this.setState({imageAsFile:image});
      // console.log('state apicall', this.state);
  
      const storage = firebase.storage();
  
      if (image === '') {
        console.log("Not an image")
      }
  
      const uploadTask = storage.ref(`/CutomAdsData/${image.name}`).put(image)
      console.log("uploadTask", uploadTask);
  
      uploadTask.on('state_changed',
        (snapShot) => {
          //takes a snap shot of the process as it is happening
          console.log(snapShot)
        }, (err) => {
          //catches the errors
          console.log(err)
        }, () => {
          // gets the functions from storage refences the image storage in firebase by the children
          // gets the download url then sets the image from firebase as the value for the imgUrl key:
          storage.ref('CutomAdsData').child(image.name).getDownloadURL()
            .then(async (fireBaseUrl) => {

              let dataToInsert = {
                userID: firebase.auth().currentUser.uid,
                fileName:image.name,
                fileUrl:fireBaseUrl,
                //file:infoListVal,
                textVal:textVal,
                city: cityVal,
                specialization: specializationVal,
                createdOn: new Date().toISOString(),
                createdBy: firebase.auth().currentUser.uid,
                createdByUserEmail: firebase.auth().currentUser.email,
              }
  
              console.log("submit state ads", dataToInsert)
             
              await firebase.firestore().collection("customAds")
                .add(dataToInsert)
                .then(ref => {
                  console.log("current user dataToInsert", ref);
                  //history.push('/vertical/doctors')
                  history.push('/vertical/adsList')
                })

            })

  })
}

const infoListFunc = (event) => {

console.log("info",event)
infoList(event);
setChangeN(true)

}

  return (

    <div>
      <h3 style={{ padding: 10, textAlign: 'left' }}>Add Custom Ads for Doctor TV module</h3>

      <Card>
        <Form layout='vertical'>
          <div className='row'>

            <div className='col-md-12 col-sm-12'>
            <div style={{
                        width: 1000, height: 200, borderStyle: 'dashed', borderWidth: 3,
                        borderColor: '#ddd',
                        borderRadius: 1, position: 'relative'
                      }}>
                        <div style={{ textAlign: 'center' }}>
                          <br />
                          <br />
                          <p style={{ fontSize: 20, fontFamily: 'Rockwell' }}>Drop an image here</p>

                          <p style={{ fontFamily: 'Times New Roman' }}>or Click to choose.</p>


                          <input type="file" id="imageUpload" multiple
                          style={{ width: 1000, height: 200, position: 'absolute',
                           top: 0, left: 0, textIndent: -9999, cursor: 'pointer' }} 
                           onChange={(event)=>infoListFunc(event.target.files)} />
                        </div>
                      </div>
             
            </div>
            <br/><br/>
            {infoListVal.length > 0 ? <p>{infoListVal[0].name}</p> : null}
            

          </div>
          <br/><br/>
          <div className='row'>
          <div className='col-md-12 col-sm-12'>
            <FormItem label='Ads Text to display'>
            <TextField
                id="outlined-multiline-static"
                //label="Ads Text to display"
                style={{ width: '100%' }}
                onChange={(e) => text(e.target.value)}
                value={textVal}
                multiline
                rows={4}
                placeholder="Enter Ads Text to display"
                //defaultValue="A"
                variant="outlined"
              />
              </FormItem>

            </div>
            </div>
            <br/><br/>
          <div className='row'>
          <div className='col-md-6 col-sm-12'>
            <FormItem label='Select City'>
                <Select defaultValue=""
                  onChange={(e) => city(e)}
                >
                  <Option value="Kurukshetra">Kurukshetra</Option>
                  <Option value="Karnal">Karnal</Option>
                  <Option value="Pehwa">Pehwa</Option>
                  <Option value="Ladwa">Ladwa</Option>
                  <Option value="Ambala">Ambala</Option>
                </Select>
              </FormItem>

            </div>
            <div className='col-md-6 col-sm-12'>
            <FormItem label='Select Doctor Specialization'>
                <Select defaultValue=""
                  onChange={(e) => specialization(e)}
                >
                  <Option value="Dental">Dental</Option>
                  <Option value="Ayurvedic">Ayurvedic</Option>
                  <Option value="ENT">ENT</Option>
                </Select>
              </FormItem>
            </div>
           

          </div>
         
          
          <div className='row'>
            <div className='col-md-4 col-sm-4'>

            </div>
            <div className='col-md-5 col-sm-5'>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white", width: '70%'
                }}
                onClick={saveHospitalName}
              >
                Save
                      </Button>
             
            </div>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default AdsAdd;
