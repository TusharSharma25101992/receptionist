import React, { useState, useEffect } from 'react';
import { IPageData } from '../../interfaces/page';
import { history } from '../../redux/store';
import Department from '../../layout/components/department/Department';
import Table from "@material-ui/core/Table";
// import DatePicker from "react-datepicker";
import { TextareaAutosize } from "@material-ui/core";
import moment from "moment";
import _ from 'underscore';
// import "react-datepicker/dist/react-datepicker.css";
import DeleteForeverOutlinedIcon from "@material-ui/icons/DeleteForeverOutlined";
// import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// import Checkbox from '@material-ui/core/Checkbox';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import GetAppIcon from "@material-ui/icons/GetApp";
import { useFetchPageData, usePageData } from '../../hooks/usePage';
import { IDepartment } from '../../interfaces/patient';
import {
  Alert,
  AutoComplete,
  // Button,
  Card,
  Checkbox,
  Input,
  Radio,
  Rate,
  Select,
  Switch,
  Tag,
  Form,
  DatePicker,
  Spin, Space, Row, Col, Divider, Tooltip
} from 'antd';
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import secondaryFireApp from '../../secondaryApp'

const FormItem = Form.Item;
const { Option } = Select;
// const firebaseConfig = {
//   apiKey: "AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw",
//   authDomain: "opd-check-com.firebaseapp.com",
//   projectId: "opd-check-com",
//   storageBucket: "opd-check-com.appspot.com",
//   messagingSenderId: "473581330102",
//   appId: "1:473581330102:web:1415eceae267ee888bd017",
//   measurementId: "G-HL6YYGDVB1"
// };
// const secondaryFireApp = firebase.initializeApp(firebaseConfig, "secondaryFireApp");
const pageData: IPageData = {
  title: 'Departments',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Medicine',
      route: 'default-dashboard'
    },
    {
      title: 'Departments'
    }
  ]
};

const Departments = (props) => {
  const [hospitalNameVal, hospitalName] = useState("");
  const [firstNameVal, firstName] = useState(props.data ? props.data.firstName : props.firstName);
  const [lastNameVal, lastName] = useState(props.data ? props.data.lastName : props.lastName);
  const [emailAddressVal, emailAddress] = useState(props.data ? props.data.email : props.email);
  const [passwordVal, password] = useState(props.data ? props.data.code : props.code);
  const [mobileNumberVal, mobileNumber] = useState(props.data ? props.data.mobileNumber : props.mobileNumber);

  const [change, setChange] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  const [doctorList, setDoctorList] = useState([]);
  const [doctorArr, setDoctorArr] = useState(props.data ? props.data.doctors : props.doctors);
  const [departments] = useFetchPageData<IDepartment[]>('./data/departments.json', []);
  // usePageData(pageData);

  const depClass = (i, length) => {
    if (i === length - 1) {
      return 'mb-0';
    }

    if (i > length - 4) {
      return 'mb-md-0';
    }

    return '';
  };

  useEffect(() => {


    firebase.auth().onAuthStateChanged(async (user) => {
      var user = firebase.auth().currentUser;
      if (user) {
        console.log("Add recptionist User", user);
        console.log("Add recptionist props ", props);
        let arr = [];
        hospitalName(props.data ? props.data.hospitalName : props.hospitalName)
        await firebase.firestore()
          .collection("doctors").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setDoctorList(arr)
          })
      } else {
        history.push("/")
      }
    })

  }, [props])

  console.log("doctorList", doctorList)

  const saveReceptionist = async () => {

    console.log("hospitalNameVal", doctorArr)
    
            let dataToInsertReceptionist = {
             
              mobileNumber: mobileNumberVal,
              firstName: firstNameVal,
              lastName: lastNameVal,
              email: emailAddressVal,
              code: (passwordVal.length > 0 ? passwordVal : "test1234"),
              doctors: doctorArr,
              updatedOn: new Date().toISOString(),
              
            }

           
            await firebase.firestore().collection("receptionist").doc(props.data.userID)
            .update(dataToInsertReceptionist).then(ref => {
                console.log("current user dataToInsertReceptionist", ref);
                //history.push('/vertical/doctors')
                history.push('/vertical/hospitalList')
              })
            
          }
     

  return (

    <div>
      <h3 style={{ padding: 10, textAlign: 'left' }}>Edit Receptionist Details</h3>

      <Card>
        <Form layout='vertical'>
          <div className='row'>

            <div className='col-md-12 col-sm-12'>
              <FormItem label='Hospital/Clinic Name'>
                <Input
                  onChange={(e) => hospitalName(e.target.value)}
                  placeholder='Enter Hospital/Clinic Name'
                  value={hospitalNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='First Name'>
                <Input
                  onChange={(e) => firstName(e.target.value)}
                  placeholder='First Name'
                  value={firstNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Last Name'>
                <Input

                  placeholder='Last Name'
                  onChange={(e) => lastName(e.target.value)}
                  value={lastNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Email Address'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Email Address'
                  onChange={(e) => emailAddress(e.target.value)}
                  value={emailAddressVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Password'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Password'
                  onChange={(e) => password(e.target.value)}
                  value={passwordVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>

            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Mobile Number'>
                <Input

                  placeholder='Mobile Number'
                  onChange={(e) => mobileNumber(e.target.value)}
                  value={mobileNumberVal}
                //pattern="[1-9]{1}[0-9]{9}"
                //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <Grid container spacing={6}>
            <Grid item xs={10} sm={10}>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",
                  width: 220,
                }}
                onClick={() => {
                  setChangeN(false)
                  doctorArr.push({
                    id: '',
                    name: '',
                    specialization: '',
                    opdTimings: '',
                    state: '',
                    mobileNumber: '',
                    city: '',
                  });
                  setChangeN(!changeN)

                  console.log("doctorArr", doctorArr);
                }}
              >
                Add Doctor
                      </Button>
            </Grid>
          </Grid>
          <br />
          <TableContainer style={{ marginBottom: 20 }}>
            <Table aria-label="customized table" size="small">
              <TableHead>
                <TableRow style={{ backgroundColor: "#ebedef" }}>
                  <TableCell
                    style={{ border: "1px solid black" }}
                    align="center"
                  >
                    <b>Doctor Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Delete</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {doctorArr.length > 0 &&
                  doctorArr.map((data, idx) => (
                    <TableRow key={idx}>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                        <Select
                          defaultValue={doctorArr[idx].name}
                          onChange={(e) => {
                            console.log("hospitalArr change", JSON.parse(e));

                            doctorArr[idx].id = JSON.parse(e).userID;
                            doctorArr[idx].name = JSON.parse(e).firstName + " " + JSON.parse(e).lastName;
                            doctorArr[idx].specialization = JSON.parse(e).specialization;
                            doctorArr[idx].opdTimings = JSON.parse(e).opdTimings;
                            doctorArr[idx].state = JSON.parse(e).state;
                            doctorArr[idx].mobileNumber = JSON.parse(e).mobileNumber;
                            doctorArr[idx].city = JSON.parse(e).city;
                            setChangeN(!changeN)
                          }
                          }
                        >

                          {doctorList.length > 0 && doctorList.map((item, index) => (
                            <Option
                              selected={
                                item.firstName == doctorArr[idx].name
                                  ? true
                                  : false
                              }
                              value={JSON.stringify(item)}>{item.firstName + ' ' + item.lastName}</Option>
                          ))}

                        </Select>
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                        <IconButton
                          style={{ padding: "0!important", height: 40 }}
                          edge="end"
                          aria-label="clear"
                          onClick={() => {
                            console.log("idx", idx);
                            console.log("hospitalArr before", doctorArr);
                            doctorArr.splice(idx, 1);
                            // console.log("hospitalArr", hospitalArr);
                            setDoctorArr(doctorArr);
                            console.log("doctorArr", doctorArr);
                            setChangeNN(!changeNN);
                          }}
                        >
                          <DeleteForeverOutlinedIcon />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br />
          <div className='row'>
            <div className='col-md-4 col-sm-4'>

            </div>
            <div className='col-md-5 col-sm-5'>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white", width: '70%'
                }}
                onClick={saveReceptionist}
              >
                Save
                      </Button>
              {/* <Button type='primary' style={{ width: '70%',backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white" }}
                  onClick={saveReceptionist}
                >Save</Button> */}
            </div>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default Departments;
