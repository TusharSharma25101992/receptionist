import React, { useState, useEffect } from 'react';
import { Avatar, Table, Button, Modal, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { Spin, Space, Row, Col, Divider,Tooltip,Input } from 'antd';
// import { IPatient } from '../../../interfaces/patient';
import {

    PlusCircleTwoTone,
  } from '@ant-design/icons';
import { history } from '../../redux/store';
// import PatientForm from '../../../layout/components/patients/PatientForm';

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

// type Props = {
//   patients: IPatient[];
//   onEditPatient?: (patient: IPatient) => void;
//   onDeletePatient?: (id: string) => void;
// };

// type PatientsImgProps = {
//   img: string;
// };

// const PatientImg = ({ img }: PatientsImgProps) => {
//   const isData = img.startsWith('data:image');
//   const isWithPath = img.startsWith('http');

//   if (isData || isWithPath) {
//     return <Avatar size={40} src={img} />;
//   }

//   return <Avatar size={40} src={`${window.location.origin}/${img}`} />;
// };

const HospitalList = () => {

  const [deleteData, setDeleteData] = useState("");
  const [patient, setPatient] = useState(null);
  const [visibility, setVisibility] = useState(false);
  const [change, setChange] = useState(false);
  const [docList, setDoctorList] = useState([]);

  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [modalText, setModalText] = React.useState('Are you sure you want to delete this record?');


  useEffect(() => {

    getData()

  }, [])

  const getData = () => {

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user id", user.uid);

        let arr = [];
        await firebase.firestore()
          .collection("hospitals").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setDoctorList(arr)
          });

      }
    })

  }

  console.log("docList", docList);

  const showModal = (data) => {
    setDeleteData("")
    console.log("data inside modal",data)
    setDeleteData(data.userID)
    setVisible(true);
  };

  const handleOk = async () => {
    console.log("deleteData",deleteData)
    // let uid = deleteData.userId;
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);

    // await firebase.firestore().collection("users").doc(userf.uid);
    await firebase.firestore().collection("hospitals").doc(deleteData).delete().then(() => {
      console.log("Document successfully deleted!");
      setVisible(false);
      setConfirmLoading(false)
      // window.location.reload();
      history.push('/vertical/hospitalList')
      // getData();
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
    // setTimeout(() => {
    //   setVisible(false);
    //   setConfirmLoading(false);
    // }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  const handleEditPatient = (data) => {
    console.log('data',data)
    // history.push('/vertical/formElementsEdit');
    history.push({
      pathname: "/vertical/hospitalEdit",
      state: data,
    });

  }

  const closeModal = () => setVisibility(false);

//   const handleShowInfo = () => history.push('/vertical/patient-profile');
//   const handleDeletePatient = (id) => onDeletePatient(id);


  const actions = (data) => (
   
    <div className='buttons-list nowrap'>
      {/* <Button shape='circle' onClick={handleShowInfo}>
        <span className='icofont icofont-external-link' />
      </Button> */}
      <Button
        onClick={()=>handleEditPatient(data)} 
        shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button
        onClick={()=>showModal(data)}
        shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
      <Modal
        title=""
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>{modalText}</p>
      </Modal>
    </div>
  );
    

  const columns = [
    // {
    //   key: 'img',
    //   title: 'Photo',
    //   dataIndex: 'img',
    //   render: (img) => <PatientImg img={img} />
    // },
    // {
    //     key: 'userID',
    //     dataIndex: 'userID',
    //     title: 'User ID',
    //     sorter: (a, b) => a.userID.length - b.userID.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (userID) => <strong>{userID}</strong>
    //   },
    {
      key: 'hospitalName',
      dataIndex: 'hospitalName',
      title: 'Hospital/Clinic Name',
      sorter: (a, b) => a.hospitalName.length - b.hospitalName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (hospitalName) => <strong>{hospitalName}</strong>
    },
    {
      key: 'firstName',
      dataIndex: 'firstName',
      title: 'First Name',
      sorter: (a, b) => a.firstName.length - b.firstName.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (firstName) => <strong>{firstName}</strong>
    },
    {
        key: 'lastName',
        dataIndex: 'lastName',
        title: 'Last Name',
        sorter: (a, b) => a.lastName.length - b.lastName.length,
        // sorter: (a, b) => (a.name > b.name ? 1 : -1),
        render: (lastName) => <strong>{lastName}</strong>
      },
      {
        key: 'email',
        dataIndex: 'email',
        title: 'Email',
        sorter: (a, b) => a.email.length - b.email.length,
        // sorter: (a, b) => (a.name > b.name ? 1 : -1),
        render: (email) => <strong>{email}</strong>
      },
    //   {
    //     key: 'code',
    //     dataIndex: 'code',
    //     title: 'Password',
    //     sorter: (a, b) => a.code.length - b.code.length,
    //     // sorter: (a, b) => (a.name > b.name ? 1 : -1),
    //     render: (code) => <strong>{code}</strong>
    //   },
      {
      key: 'mobileNumber',
      dataIndex: 'mobileNumber',
      title: 'Mobile Number',
      sorter: (a, b) => a.mobileNumber.length - b.mobileNumber.length,
      // sorter: (a, b) => (a.name > b.name ? 1 : -1),
      render: (mobileNumber) => <strong>{mobileNumber}</strong>
    },
    
    {
      key: 'actions',
      title: 'Actions',
      render: actions
    }
  ];

  const pagination = docList.length <= 10 ? false : {};

  function addEmployee(){

    console.log("addEmployee");

    history.push({ pathname: `/vertical/departments`, state: {} });

  }

  return (
    <>
    <Row>
          <Col span={7}>
            <h3 style={{ padding: 10, textAlign: 'left' }}>Hospital / Clinic List</h3>
          </Col>
          <Col span={7}>
            <Tooltip placement="top" title="Add Hospital / Clinic">
              <PlusCircleTwoTone style={{ fontSize: 25, marginTop: 40, cursor: 'pointer' }} 
              onClick={addEmployee}
               />
            </Tooltip>

          </Col>
          <Col span={10}>

            <Input placeholder='Type to search' suffix={<span className='icofont icofont-search' />}
              style={{ width: '60%', float: 'right', marginTop: 40 }}
             // onChange={this.search}
            />

          </Col>

        </Row>
      <Table
        pagination={pagination}
        className='accent-header'
        rowKey='id'
        dataSource={docList}
        columns={columns}
      />

      {/* <Modal
        visible={visibility}
        footer={null}
        onCancel={closeModal}
        title={<h3 className='title'>Add patient</h3>}
      >
        <PatientForm
          submitText='Update patient'
          onCancel={closeModal}
          onSubmit={onEditPatient}
          patient={patient}
        />
      </Modal> */}
    </>
  );
};

export default HospitalList;
