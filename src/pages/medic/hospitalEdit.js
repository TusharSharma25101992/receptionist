import React, { useState, useEffect } from 'react';
import { IPageData } from '../../interfaces/page';
import { history } from '../../redux/store';
import Department from '../../layout/components/department/Department';
import { useLocation } from 'react-router-dom';
// import { useFetchPageData, usePageData } from '../../hooks/usePage';
// import { IDepartment } from '../../interfaces/patient';
import EditIcon from '@material-ui/icons/Edit';
import ListAltIcon from '@material-ui/icons/ListAlt';
import {
  Alert,
  AutoComplete,
  //Button,
  Card,
  Checkbox,
  Input,
  Radio,
  Rate,
  Select,
  Switch,
  Tag,
  Form,
  DatePicker,
  Spin, Space, Row, Col, Divider, Tooltip
} from 'antd';
import Table from "@material-ui/core/Table";
// import DatePicker from "react-datepicker";
import { TextareaAutosize } from "@material-ui/core";
import moment from "moment";
// import "react-datepicker/dist/react-datepicker.css";
import DeleteForeverOutlinedIcon from "@material-ui/icons/DeleteForeverOutlined";
// import AddCircleRounded from "@material-ui/icons/AddCircleRounded";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// import Checkbox from '@material-ui/core/Checkbox';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
// import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import GetAppIcon from "@material-ui/icons/GetApp";
import { useFetchPageData, usePageData } from '../../hooks/usePage';
import { IDepartment } from '../../interfaces/patient';
import AddReceptionist from "./AddReceptionist";
import EditReceptionist from "./EditReceptionist";
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import secondaryFireApp from '../../secondaryApp'

const FormItem = Form.Item;
const { Option } = Select;
// const firebaseConfig = {
//   apiKey: "AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw",
//   authDomain: "opd-check-com.firebaseapp.com",
//   projectId: "opd-check-com",
//   storageBucket: "opd-check-com.appspot.com",
//   messagingSenderId: "473581330102",
//   appId: "1:473581330102:web:1415eceae267ee888bd017",
//   measurementId: "G-HL6YYGDVB1"
// };
// const secondaryFireApp = firebase.initializeApp(firebaseConfig, "secondaryFireApp");
// const pageData: IPageData = {
//   title: 'Departments',
//   fulFilled: false,
//   breadcrumbs: [
//     {
//       title: 'Medicine',
//       route: 'default-dashboard'
//     },
//     {
//       title: 'Departments'
//     }
//   ]
// };

const HospitalEdit = () => {

  const location = useLocation();
  let data = location.state;
  // usePageData(pageData);
  console.log('props data', data);
  const [open, setOpen] = React.useState(false);

  const [hospitalNameVal, hospitalName] = useState(data.hospitalName);
  const [firstNameVal, firstName] = useState(data.firstName);
  const [lastNameVal, lastName] = useState(data.lastName);
  const [emailAddressVal, emailAddress] = useState(data.email);
  const [passwordVal, password] = useState(data.code);
  const [mobileNumberVal, mobileNumber] = useState(data.mobileNumber);

  const [change, setChange] = useState(false);
  
  const [addReceptionist, setReceptionist] = useState(false);
  const [editReceptionist, setReceptionistEdit] = useState(false);
  const [changeN, setChangeN] = useState(false);
  const [changeNN, setChangeNN] = useState(false);
  const [doctorList, setDoctorList] = useState([]);
  const [doctorArr, setDoctorArr] = useState(data.doctors);
  const [changeArr, setChangeArr] = useState(false);
  const [receptionistArr, setReceptionistArr] = useState([]);
  const [recptionistDoctorArr, setRecptionistDoctorArr] = useState([]);
  const [receptionistEdit, setRecptionistEdit] = useState({});


  // const [hospitalNameVal, hospitalName] = useState("");
  // const [firstNameVal, firstName] = useState("");
  // const [lastNameVal, lastName] = useState("");
  // const [emailAddressVal, emailAddress] = useState("");
  // const [passwordVal, password] = useState("");
  // const [mobileNumberVal, mobileNumber] = useState("");

  // const [change, setChange] = useState(false);
  // const [changeN, setChangeN] = useState(false);
  // const [changeNN, setChangeNN] = useState(false);
  // const [doctorList, setDoctorList] = useState([]);
  // const [doctorArr, setDoctorArr] = useState([
  //   {
  //     id: '',
  //     name: ''
  //   }
  // ]);

  //   const [departments] = useFetchPageData<IDepartment[]>('./data/departments.json', []);
  // usePageData(pageData);

  const handleClickOpen = (data) => {
    setOpen(true);
    console.log("data handle",data)
    setRecptionistDoctorArr(data.doctors)
  };

  const handleClose = () => {
    setOpen(false);
  };


  const depClass = (i, length) => {
    if (i === length - 1) {
      return 'mb-0';
    }

    if (i > length - 4) {
      return 'mb-md-0';
    }

    return '';
  };
  useEffect(() => {

    var user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        console.log("user", user);
        let arr = [];
        await firebase.firestore()
          .collection("doctors").get().then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {

              console.log("doc", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arr.push(doc.data())

            });
            setChange(true)
            setDoctorList(arr)
          })
      }
    })

    getHospitalReceptionist()

  }, [])

  console.log("doctorList", doctorList)


  const handleRecpEdit = (data) =>{
    console.log("handleRecpEdit",data)
    setRecptionistEdit(data)
    setReceptionistEdit(true);

  }
  


  const getHospitalReceptionist = async () => {

    console.log("data.userID", data.userID)

    let arrNew = [];
        await firebase.firestore()
        .collection("receptionist").where('hospitalId','==',data.userID).get()
          .then((querySnapshot) =>{
            // console.log("querySnapshot",querySnapshot)
            querySnapshot.forEach( (doc)=> {

              console.log("doc receptionist", doc.data());
              // doc.data() is never undefined for query doc snapshots

              arrNew.push(doc.data())

            });

          
            setChangeArr(true)
            setReceptionistArr(arrNew)

          })
  }

  console.log("receptionistArr", receptionistArr)

  const UpdateHospitalValue = async () => {

    console.log("hospitalNameVal", hospitalNameVal)
    // if(hospitalNameVal.length > 0){

    //   firebase.auth().onAuthStateChanged(async (user) => {
    //     console.log("user add hospital page",user)
    //     secondaryFireApp
    //     .auth()
    //     .createUserWithEmailAndPassword(emailAddressVal, (passwordVal.length > 0 ? passwordVal : "test1234"))
    //     .then(async (res) => {
    //       console.log("res", res)

    //       var userf = secondaryFireApp.auth().currentUser;
    //       console.log("userf", userf)

    // let dataToInsertUser = {
    //     userID:userf.uid,
    //     role:"hospital",
    //     firstName: firstNameVal,
    //     lastName: lastNameVal,
    //     email: emailAddressVal,
    //     code: (passwordVal.length > 0 ? passwordVal : "test1234"),
    //     mobileNumber: mobileNumberVal,
    //     createdOn: new Date().toISOString(),
    //     createdBy: firebase.auth().currentUser.uid,
    //     createdByUserEmail: firebase.auth().currentUser.email,
    //   }
    let dataToUpdate = {
      // userID:userf.uid,
      // role:"hospital",
      hospitalName: hospitalNameVal,
      mobileNumber: mobileNumberVal,
      firstName: firstNameVal,
      lastName: lastNameVal,
      email: emailAddressVal,
      code: (passwordVal.length > 0 ? passwordVal : "test1234"),
      // doctors: doctorArr,
      doctors: [],
      // patients: [],
      // Clinic_Timing: "",
      // createdOn: new Date().toISOString(),
      // createdBy: firebase.auth().currentUser.uid,
      // createdByUserEmail: firebase.auth().currentUser.email,
      updated_on: new Date().toISOString()
    }
    console.log('submit state ', dataToUpdate);
    // console.log("submit state ", dataToInsert)
    await firebase
      .firestore()
      .collection('hospitals')
      .doc(data.userID)
      .update(dataToUpdate)
      .then((ref) => {
        console.log('current user updated!');
        history.push('/vertical/hospitalList');
      });

    // })

    // }else{

    //   alert("please enter hospital or clinic name!")
    // }



  }
  if (addReceptionist) {
    return <AddReceptionist data={data} />
  }

  if (editReceptionist) {
    return <EditReceptionist data={receptionistEdit} />
  }

  return (

    <div>
      <h3 style={{ padding: 10, textAlign: 'left' }}>Edit Hospital/Clinic</h3>

      <Card>
        <Form layout='vertical'>
          <div className='row'>

            <div className='col-md-12 col-sm-12'>
              <FormItem label='Hospital/Clinic Name'>
                <Input
                  onChange={(e) => hospitalName(e.target.value)}
                  placeholder='Enter Hospital/Clinic Name'
                  value={hospitalNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='First Name'>
                <Input
                  onChange={(e) => firstName(e.target.value)}
                  placeholder='First Name'
                  value={firstNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Last Name'>
                <Input

                  placeholder='Last Name'
                  onChange={(e) => lastName(e.target.value)}
                  value={lastNameVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Email Address'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Email Address'
                  onChange={(e) => emailAddress(e.target.value)}
                  value={emailAddressVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
            <div className='col-md-6 col-sm-12'>
              <FormItem label='Password'>
                <Input
                  // onChange={(event) => altMobile(event)}
                  placeholder='Password'
                  onChange={(e) => password(e.target.value)}
                  value={passwordVal}
                //prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>

            </div>

          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-6'>
              <FormItem label='Mobile Number'>
                <Input

                  placeholder='Mobile Number'
                  onChange={(e) => mobileNumber(e.target.value)}
                  value={mobileNumberVal}
                //pattern="[1-9]{1}[0-9]{9}"
                //prefix={<MailOutlined  style={{ color: 'rgba(0,0,0,.25)' }} />}
                />
              </FormItem>
            </div>
          </div>
          <br />
          <Grid container spacing={6}>
            {/* <Grid item xs={10} sm={10}>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",
                  width: 220,
                }}
                onClick={() => {
                  setChangeN(false)
                  doctorArr.push({
                    id: '',
                    name: ''
                  });
                  setChangeN(!changeN)

                  console.log("doctorArr", doctorArr);
                }}
              >
                Add Doctor
                      </Button>
            </Grid> */}
            <Grid item xs={10} sm={10}>
              <Button
                variant="contained"
                //  disabled={!this.state.section4}
                style={{
                  backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white",
                  width: 220,
                }}
                onClick={() => {
                  setChangeN(false)
                  setReceptionist(true)
                  setChangeN(!changeN)

                  console.log("doctorArr", doctorArr);
                }}
              >
                Add Receptionist
                      </Button>
            </Grid>
          </Grid>
          <br />
          <TableContainer style={{ marginBottom: 20 }}>
            <Table aria-label="customized table" size="small">
              <TableHead>
                <TableRow style={{ backgroundColor: "#ebedef" }}>
                  <TableCell
                    style={{ border: "1px solid black",width: '20%' }}
                    align="center"
                  >
                    <b>Receptionist First Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Receptionist Last Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Email</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Phone Number</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '10%' }}
                    align="center"
                  >
                    <b>Edit Details</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Doctors List</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {receptionistArr.length > 0 &&
                  receptionistArr.map((data, idx) => (
                    <TableRow key={idx}>
                      <TableCell
                        style={{ border: "1px solid black" }}
                        align="center"
                      >
                        {data.firstName}
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                        {data.lastName}
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                        {data.email}
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                        {data.mobileNumber}
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                          <Tooltip placement="top" title="Edit">
                        <IconButton
                                  style={{ padding: "0!important", height: 40 }}
                                  edge="end"
                                  aria-label="clear"
                                  onClick={()=>handleRecpEdit(data)}
                                >
                                  <EditIcon />
                                </IconButton>
                                </Tooltip>

                               
                      </TableCell>
                      <TableCell style={{ border: "1px solid black" }}
                        align="center">
                          <Tooltip placement="top" title="Doctor List">
                         <IconButton
                                  style={{ padding: "0!important", height: 40 }}
                                  edge="end"
                                  aria-label="clear"
                                  onClick={()=>handleClickOpen(data)}
                                >
                                  <ListAltIcon />
                                </IconButton>
                                </Tooltip>
                                <Dialog
                                maxWidth="lg"
        open={open}
        //onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Doctor's List"}</DialogTitle>
        <DialogContent>
          <TableContainer style={{ marginBottom: 20 }}>
            <Table aria-label="customized table" size="small">
              <TableHead>
                <TableRow style={{ backgroundColor: "#ebedef" }}>
                  <TableCell
                    style={{ border: "1px solid black" }}
                    align="center"
                  >
                    <b>Doctor Name</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Specializaion</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>OPD Timings</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>City</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>State</b>
                  </TableCell>
                  <TableCell
                    style={{ border: "1px solid black", width: '20%' }}
                    align="center"
                  >
                    <b>Mobile Number</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {recptionistDoctorArr.length > 0 &&
                  recptionistDoctorArr.map((data, idx) => (
                    <TableRow key={idx}>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                       {data.name}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                         {data.specialization}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                         {data.opdTimings}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                         {data.city}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                         {data.state}
                      </TableCell>
                      <TableCell
                        style={{ border: "1px solid black", width: '50%' }}
                        align="center"
                      >
                         {data.mobileNumber}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </DialogContent>
        <DialogActions>
          {/* <Button onClick={handleClose} color="primary">
            Update
          </Button> */}
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br />
          <div className='row'>
            <div className='col-md-4 col-sm-4'>

            </div>

            <br />
            <br />
            <div className='col-md-5 col-sm-5'>
              <Button variant="contained" style={{
                backgroundColor: "rgba(255,122,90,0.90)",
                color: "white", width: '70%'
              }} onClick={UpdateHospitalValue}>
                Update
              </Button>
              {/* <Button type='primary' style={{ width: '70%',backgroundColor: "rgba(255,122,90,0.90)",
                  color: "white" }}
                  onClick={UpdateHospitalValue}
                >Update</Button> */}
            </div>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default HospitalEdit;
