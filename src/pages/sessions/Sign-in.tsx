import React,{useState} from 'react';

import { Button, Form, Input, Switch } from 'antd';
import { LoginOutlined } from '@ant-design/icons/lib';

import PublicLayout from '../../layout/public/Public';
import { Link } from 'react-router-dom';
import { useForm } from 'antd/es/form/Form';
import { navigateHome } from '../../utils/naviagate-home';
import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

const { Item } = Form;

const SignIn = () => {
  const [form] = useForm();
  const [userNameVal,userName] = useState("")
  const [passwordVal,password] = useState("")

  const login = () => {

    firebase.auth().signInWithEmailAndPassword(userNameVal,passwordVal)
    .then((user)=>{

      console.log("login user",user)
      if(user){

        navigateHome();

      }

    })

    // form
    //   .validateFields()
    //   .then(() => navigateHome())
    //   .catch(() => null);
  };

  

  return (
    <PublicLayout bgImg={`${window.origin}/content/login-page.jpg`}>
      <h4 className='mt-0 mb-1'>Doctor / Receptionist Login</h4>
      
      <p className='text-color-200'>Login to access your Account</p>

      <Form form={form} layout='vertical' className='mb-4'>
        <Item name='login' 
         rules={[{ required: true, message: <></> }]}>
          <Input 
          onChange={(e) => userName(e.target.value)}
          value={userNameVal}
          placeholder='Login' />
        </Item>
        <Item name='password' rules={[{ required: true, message: <></> }]}>
          <Input 
          onChange={(e) => password(e.target.value)}
          value={passwordVal}
          placeholder='Password' type='password' />
        </Item>

        <div className='d-flex align-items-center mb-4'>
          <Switch defaultChecked /> <span className='ml-2'>Remember me</span>
        </div>

        <Button
          block={false}
          type='primary'
          onClick={login}
          htmlType='submit'
          icon={<LoginOutlined style={{ fontSize: '1.3rem' }} />}
        >
          Login
        </Button>
      </Form>
      <br />
      <p className='mb-1'>
        <a href='#'>Forgot password?</a>
      </p>

      <p>
        Don't have an account? <Link to='sign-up'>Sign up!</Link>
      </p>
    </PublicLayout>
  );
};

export default SignIn;
