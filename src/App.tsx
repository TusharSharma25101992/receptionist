import React, { useEffect, useState } from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import VerticalLayout from './layout/vertical/Vertical';
import HorizontalLayout from './layout/horizontal/Horizontal';

import NotFound from './pages/sessions/404';
import { defaultRoutes, sessionRoutes } from './routing';

import './App.scss';
import { useHideLoader } from './hooks/useHideLoader';

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';


const Routes = ({ routes, layout = '' }) => (
  <Switch>
    {routes.map((route, index) => (
      <Route
        key={index}
        exact={route.exact}
        path={layout.length > 0 ? `/${layout}/${route.path}` : `/${route.path}\``}
        component={() => <route.component />}
      />
    ))}

    <Route path='*'>
      <Redirect to='/public/page-404' />
    </Route>
  </Switch>
);

const DefaultRoutes = ({ layout }) => <Routes routes={defaultRoutes} layout={layout} />;

const SessionRoutes = () => <Routes routes={sessionRoutes} layout='public' />;

const App = () => {
  useHideLoader();

  const [fireAppInitializedVal,fireAppInitialized] = useState("")
  const firebaseConfigLogin = {
    apiKey: "AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw",
    authDomain: "opd-check-com.firebaseapp.com",
    projectId: "opd-check-com",
    storageBucket: "opd-check-com.appspot.com",
    messagingSenderId: "473581330102",
    appId: "1:473581330102:web:1415eceae267ee888bd017",
    measurementId: "G-HL6YYGDVB1"
  };
  firebase.initializeApp(firebaseConfigLogin);
  // var secondaryFireApp = "";

  // var secondaryFireApp = "";
  useEffect(() => {


    // console.log("secondaryFireApp",secondaryFireApp)
    // fireAppInitialized(secondaryFireApp)
        setTimeout(() => {
            // let uid = null
            firebase.auth().onAuthStateChanged(async (user) => {
                console.log("this.firebaseConfigLogin", user);
            }) 
        }, 3000);

    // const firebaseConfig = {
    //   apiKey: "AIzaSyAaSU09t-EbFYf2frdgE_7Sw5sjQAAKGGw",
    //   authDomain: "opd-check-com.firebaseapp.com",
    //   projectId: "opd-check-com",
    //   storageBucket: "opd-check-com.appspot.com",
    //   messagingSenderId: "473581330102",
    //   appId: "1:473581330102:web:1415eceae267ee888bd017",
    //   measurementId: "G-HL6YYGDVB1"
    // };
    // secondaryFireApp = firebase.initializeApp(firebaseConfig, "secondaryFireApp");
    // console.log("secondaryFireApp",secondaryFireApp)


  }, [])

  return (
    <Switch>
      <Route path='/' exact>
        <Redirect to='/vertical/default-dashboard' />
      </Route>

      <Route path='/public'>
        <SessionRoutes />
      </Route>

      <Route path='/horizontal'>
        <HorizontalLayout>
          <DefaultRoutes layout='horizontal' />
        </HorizontalLayout>
      </Route>

      <Route path='/vertical'>
        <VerticalLayout>
          <DefaultRoutes layout='vertical' />
        </VerticalLayout>
      </Route>

      <Route path='*'>
        <NotFound />
      </Route>
    </Switch>
  );
};

export default App;
